<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Http\Database\themes;


class ContactController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'contact');
        
        return view('themes/'.themes::getThemesPresent().'/user/contact')->with('arrayBase',$array);
        
	}
    
    public function contact(Request $request)
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'tks');
        
        $name = $request->input('tm_pb_contact_name_1');
        $phone = $request->input('tm_pb_contact_phone_1');
        $message = $request->input('tm_pb_contact_message_1');
        
        $today = date("Y-m-d H:m:s");
        
        $whereInsert = array(
                              'phone'=>$phone,
                              'name'=>$name,
                              'message'=>$message,
                              'status'=> 0,
                              'created_at'=> $today);
        
        DB::table('contact')->insert($whereInsert);
        
        return view('themes/'.themes::getThemesPresent().'/user/tks/index')->with('arrayBase',$array);
        
	}
}