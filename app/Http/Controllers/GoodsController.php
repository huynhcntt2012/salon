<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;

class GoodsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = "Phiếu xuất/nhập kho";
        $selecteditem = 1;
        $selectedmenu = 11;
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'bill');
        
        return view('admin/pages/stock/goods/index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$title = "Phiếu xuất/nhập kho";
        $selecteditem = 1;
        $selectedmenu = 11;
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'bill');
        
        return view('admin/pages/stock/goods/create')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
