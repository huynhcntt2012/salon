<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;


class MainController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $title = "Dashboard";

        $selecteditem = 0;
        $selectedmenu = 0;
        
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'main');
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    
    public function bill()
	{
        $title = "Phiếu Dịch Vụ";
        $selecteditem = 1;
        $selectedmenu = 1;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function customerinfo()
	{
        $title = "Tra Cứu Thông Tin Khách Hàng";

        $selecteditem = 1;
        $selectedmenu = 2;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function customerdate()
	{
        $title = "Tra Cứu Thông Tin Khách Hàng Theo Ngày";

        $selecteditem = 2;
        $selectedmenu = 2;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function customer()
	{
        $title = "Khách Hàng";

        $selecteditem = 3;
        $selectedmenu = 2;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function newhome()
	{
        $title = "Bảng Tin Chính";

        $selecteditem = 1;
        $selectedmenu = 3;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function newblog()
	{
        $title = "Bảng Tin Blog";

        $selecteditem = 2;
        $selectedmenu = 3;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function newservice()
	{
        $title = "Bảng Tin Dịch Vụ";

        $selecteditem = 3;
        $selectedmenu = 3;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function employeetour()
	{
        $title = "Tour Nhân Viên";

        $selecteditem = 1;
        $selectedmenu = 4;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function employee()
	{
        $title = "Nhân Viên";

        $selecteditem = 2;
        $selectedmenu = 4;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function servicecategory()
	{
        $title = "Dịch Vụ";

        $selecteditem = 1;
        $selectedmenu = 5;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function servicesub()
	{
        $title = "Loại Hình";

        $selecteditem = 2;
        $selectedmenu = 5;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function image()
	{
        $title = "Kho Ảnh";

        $selecteditem = 1;
        $selectedmenu = 6;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function video()
	{
        $title = "Kho Video";

        $selecteditem = 1;
        $selectedmenu = 7;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function point()
	{
        $title = "Các Đổi Điểm";

        $selecteditem = 1;
        $selectedmenu = 8;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function contact()
	{
        $title = "Phản Hồi Khách Hàng";

        $selecteditem = 2;
        $selectedmenu = 8;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function revenue()
	{
        $title = "Doanh Thu";

        $selecteditem = 1;
        $selectedmenu = 9;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function salaries()
	{
        $title = "Lương Nhân Viên";

        $selecteditem = 2;
        $selectedmenu = 9;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function revenexpen()
	{
        $title = "Các Khoản Thu Chi";

        $selecteditem = 3;
        $selectedmenu = 9;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function customersta()
	{
        $title = "Thống Kê Khách Hàng";

        $selecteditem = 4;
        $selectedmenu = 9;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    public function servicessta()
	{
        $title = "Thống Kê Dịch Vụ";

        $selecteditem = 5;
        $selectedmenu = 9;
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
	}
    
    
    
}