<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;


class MainCustomerController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function customerinfo()
	{
        $title = "Tra Cứu Thông Tin Khách Hàng";

        $selecteditem = 1;
        $selectedmenu = 2;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'customerinfo');
        
                                    
        return view('admin/pages/customer/customer')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function customerdate()
	{
        $title = "Tra Cứu Thông Tin Khách Hàng Theo Ngày";

        $selecteditem = 2;
        $selectedmenu = 2;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'customerdate');
        
        return view('admin/pages/customer/customerdate')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function customer(Request $request)
	{
        $title = "Khách Hàng";

        $selecteditem = 3;
        $selectedmenu = 2;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
         $today = date("Y-m-d H:m:s");
        $a="";
        if($request->input('phone')!= ""){
            
            if($request->file('img')){
                $img = $request->file('img')->getClientOriginalName();
                move_uploaded_file($_FILES["img"]["tmp_name"], 'resources/views/themes/salon/public/img/'.$_FILES["img"]["name"]);
            }else{
                $img = 'logo.png';
            }
            $arrayinsert = array(
                                    'name' => $request->input('name'),
                                    'phone' => $request->input('phone'),
                                    'birthday' => $request->input('birth'),
                                    'created_at' => $today,
                                    'image' => $img,
                                    'password' => md5($request->input('pass')),
                                    'note' =>  $request->input('text-content'));
            DB::table('customer')->insert($arrayinsert);
            $a="Thêm Thành Công";
        }
        
        $array = array('url' =>'customer', 'message' => $a);
        return view('admin/pages/customer/index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    
    
}