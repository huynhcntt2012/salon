<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;


class MainEmployeeController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function employeetour()
	{
        $title = "Tour Nhân Viên";

        $selecteditem = 1;
        $selectedmenu = 4;
        
        if(SessionController::checkAdmin('keyAdmin') == false || SessionController::checkAdmin('role') == 'employee'){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'employeetour');
                                    
        return view('admin/pages/employee/hoahong')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    public function employee()
	{
        $title = "Nhân Viên";

        $selecteditem = 2;
        $selectedmenu = 4;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'employee');
        
        return view('admin/pages/employee/index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    
}