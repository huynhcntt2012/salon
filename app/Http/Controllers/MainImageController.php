<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Database\themes;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\LoginController;


class MainImageController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    public function image($keyword = null)
	{
		if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $title = "Kho Hình Ảnh";

        $selecteditem = 1;
        $selectedmenu = 6;
        $keyword=Input::get('keyword');
        $img= DB::table('images')->where("title","LIKE","%".$keyword."%");
        $data= $img->orderBy('ID','DESC')->paginate(10);
        foreach ($data as $key) {
        	$key->type = DB::table('category')->where('id', $key->id_type)->get();
        }
        
        return view('admin.pages.imageshome' )->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('keyword',$keyword)
                                    ->with('lastPage',$data->lastPage()) 
                                	->with('currentPage',$data->currentPage())
                                    ->with('data', $data);
	}
    
    public function create(Request $request)
	{
		if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $title = "Kho Hình Ảnh";

        $selecteditem = 1;
        $selectedmenu = 6;
        $today = date("Y-m-d H:m:s");
        
        $whereInsert = array(
                              'title'=>$request->input('title'),
                              'name'=>$request->input('name-img'),
                              'id_type'=>$request->input('cate'),
                              'link_image'=> $request->input('name-img'),
                              'creater'=>'admin',
                              'created_at' => $today );
        
        DB::table('images')->insert($whereInsert);
        
        return Redirect::to('/admin/image/');
	}
    
    

	public function upload()
	{
		$title = "Upload Ảnh";

		$selecteditem = 1;
        $selectedmenu = 6;
        
        $category= DB::table('category')->get();
        
        return view('admin.pages.uploadimage.index')->with('title',$title)
		        							->with('selecteditem',$selecteditem)
		                                    ->with('selectedmenu',$selectedmenu)
                                            ->with('category',$category);
	}

    public function edit($id)
    {
    	if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $title = "Kho Hình Ảnh";

        $selecteditem = 1;
        $selectedmenu = 6;
        $id = Input::get('id');
        // $img = DB::table('images')->

    }
    public static function destroy($id)
    {
        // DB::table('images')->where(array('id' => $id))->delete();
        return redirect('admin/image');
    }
    
}