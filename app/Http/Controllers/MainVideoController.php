<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Database\themes;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\LoginController;

class MainVideoController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function video($keyword = null)
	{
        $title = "Kho Video";

        $selecteditem = 1;
        $selectedmenu = 7;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $keyword=Input::get('keyword');
        $videos= DB::table('videos')->where("title","LIKE","%".$keyword."%");
        $data= $videos->orderBy('ID','DESC')->paginate(10);
        return view('admin.pages.video.index')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('keyword',$keyword)
                                    ->with('lastPage',$data->lastPage()) 
                                	->with('currentPage',$data->currentPage())
                                    ->with('data', $data);
	}
    
    public function upload()
    {
        $title = "Kho Video";

        $selecteditem = 1;
        $selectedmenu = 7;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $keyword=Input::get('keyword');
        $videos= DB::table('videos')->where("title","LIKE","%".$keyword."%");
        $data= $videos->orderBy('ID','DESC')->paginate(10);
        return view('admin.pages.video.upload')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu);
    }
}