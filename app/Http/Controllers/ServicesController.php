<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Http\Database\themes;

class ServicesController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $data = DB::table('posts')->where(array('post_status' => 'service'))->get();
        
        $array = array('themes'=> $nameThemes,'url' =>'services');

        return view('themes/'.$nameThemes.'/user/services')->with('arrayBase',$array)
                                                            ->with('data',$data);
	}
}