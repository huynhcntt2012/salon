    <?php
    use Illuminate\Http\Request;
    
    
    
    Route::get('admin/apiPhone', 'ApiPhoneController@index');
    
    Route::get('admin/apiPhone/getPost', 'ApiPhoneController@getLoad');
    
    
    Route::post('admin/apiPhone/postdevice', 'ApiPhoneController@device');
    
    Route::get('admin/apiPhone/getdevice', 'ApiPhoneController@getDevice');
    
    Route::post('admin/apiPhone/sendnotifition', 'ApiPhoneController@Send');
    
    ?>