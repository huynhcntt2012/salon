<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Orders;
use Carbon\Carbon;
use App\Models\Employee_orders;


class Bills extends Model {

	protected $table = 'bills';
	// const id = 'stt';
	// protected $id = 'stt';
	public static function store($data) 
    {	
        $today = date("dmy");

    	$bill = new Bills;
    	$bill->status = 1;
        $bill->discount_rank = 0;
    	$bill->save();
        $countBillofDay =  null;
        $countBillofDay = Bills::whereRaw('Date(created_at) = CURDATE()')->get();
    	$bill->name = 'HD'.$today.'-'.(count($countBillofDay));
    	$bill->save();
    }

    public static function destroy($id)
    {
    	$bill = Bills::where('id', $id)->where('status', 1)->first();
    	$bill->delete();
    	$arrayIdOrder = Orders::where('bill_id', $id)->get();
        for ($i=0; $i < count($arrayIdOrder); $i++) { 
            $employee =  DB::table('employee_order')->where('order_id', $arrayIdOrder[$i]->id);
            $employee->delete();   
        }
        $order = Orders::where('bill_id', $id);
    	$order->delete();
    }

    /**
     * [update description]
     * @param  [type] $id   [description]
     * @param  [type] $data {id_customer,id_employee,status,orders=[]};
     */
    public static function updateBill($id, $data)
    {	
        $customer_id = null;
        $discount_rank = null;
        $discount_money = null;
        if ($data['customer'] != null) {
            $customer_id = $data['customer'][0];
        }
        
        if ($data->money) {
            $discount_money = $data->discountMoney;
        }
        
        if ($data->rank) {
            $discount_rank = $data->discountRank;
        }
     	$status = $data['status'];
     	Bills::where('id', $id)->update([
     			'customer_id' 		=> $customer_id,
                'discount_rank'     => $discount_rank,
                'discount_money'    => $discount_money,
     			'status'			=> $data['status']
     		]);
        
        if ($data['status'] == 2) {
            $customerOfBilll = DB::table('customer')->select('point_use', 'point_gift', 'point_acc', 'point')->where('id', $customer_id)->first();
            $pointUse = $customerOfBilll->point_use + $data['pointUse'];
            $point = $customerOfBilll->point_gift + $customerOfBilll->point_acc + $pointUse;
            DB::table('customer')->where('id', $customer_id)->update([
                                                                    'point_use' => $pointUse,
                                                                    'point'     => $point 
                                                                ]);
        }

        $arrayIdOrder = DB::table('orders')->select('id')->where('bill_id', $id)->get();
        if (count($arrayIdOrder) > 0) {
            for ($j=0; $j < count($arrayIdOrder); $j++) {
                $employee_orders = DB::table('employee_order')->where('order_id', $arrayIdOrder[$j]->id)->first();
                if ($employee_orders) {
                    DB::table('employee_order')->where('order_id', $arrayIdOrder[$j]->id)->delete();
                }
            }
        }
        $orders = Orders::where('bill_id', $id);
        $orders->delete();
        if (count($data['orders']) > 0) {
            for ($i=0; $i < count($data['orders']); $i++) { 
                $order = new Orders;
                $order->bill_id = $id;
                $order->service_id = $data['orders'][$i]['id'];
                $order->save();
                if (count($data['orders'][$i]['employees']) > 0) {
                    for ($j=0; $j < count($data['orders'][$i]['employees']); $j++) { 
                        $employee = array(
                                'employee_id'       => $data['orders'][$i]['employees'][$j]['id'],
                                'order_id'          => $order->id,
                            );
                        DB::table('employee_order')->insert($employee);
                    }
                }
            }
        } 
    }
}
