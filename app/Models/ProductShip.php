<?php 
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class ProductShip extends Model{
    
    const TABLE_PRODUCTSHIP = 'detailrelationship';
    
    const PRODUCTSHIP_ID = 'Idproduct';
    
    const PRODUCTSHIP_RLS = 'Idproduct_rls';
    
    const PRODUCTSHIPID = 'id';
    
    private $id = 'id';
    
    private $idproduct = 'Idproduct';
    
    private $idproduct_rls = 'Idproduct_rls';
    
    public function __construct(){
        
    }
    
    public function setId($id){
        $this->id = $id;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function setName($idproduct){
        $this->idproduct = $idproduct;
    }
    
    public function getName(){
        return $this->idproduct;
    }
    
}
?>