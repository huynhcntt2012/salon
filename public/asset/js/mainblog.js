var url = $("#url").val();
function setImgS(){
    var imgName = $("#name-img-upload").val();
    $("#name-img").val(imgName);
    var part =url+"/public/images/"+imgName;
    var text = "<img id='img_represent' src='"+part+"' class='img-responsive' alt='Cinque Terre'>";
    $(".box-img-rep").html(text);
    $('#myModal').modal('hide');
}
function changeRadioImg(){
    var name  = $("input[type='radio'][name='img_check']:checked").val();
    $("#name-img-upload").val(name);
}
function getImgaes(){
    $.ajax({
       url: url+'/getImages',      
       success : function(reponse) {
           var obj = JSON.parse(reponse);
          
           var name = $("#name-img-upload").val();
           var text = "";
           for (i = 0; i < obj.length; i++) { 
                var part = url+'/public/images/'+obj[i].name;
                
                text += "<div class='col-lg-3 box-img'>"+
                            "<img src='"+part+"' class='img-responsive' alt='Cinque Terre'>"+
                            "<div class='checkbox_img'>";
                if(obj[i].name == name){
                    text+="<input type='radio' name='img_check' onclick='changeRadioImg()' value='"+obj[i].name+"' checked class='checkbox'/>";
                }else{
                    text+="<input type='radio' name='img_check' onclick='changeRadioImg()' value='"+obj[i].name+"'  class='checkbox'/>";
                }
                 text+= "</div>"+"</div>";
            }
            $("#box-imgs").html(text);
       }
    });
}
function deleted(id){
    console.log(id);
    var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        return true;
                    }
                    else {

                        event.preventDefault();
                        return false;
                    }
}
function upload(){
    $("#load_screen").css("display","block");
    $("#loading").css("display","block");
    var formData = new FormData();
    formData.append('file', $('#img')[0].files[0]);
    $.ajax({
            url : url+'/image-upload',
            type : 'POST',
            data : formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(reponse) {
                var obj = JSON.parse(reponse);               
                if(obj.status==0){
                    $("#name-img-upload").val(obj.imageName);
                    $(".status-upload ").text("Update success");
                }else{
                    $(".status-upload ").text("Update fail");
                }
                $(".status-upload ").css("display","inline-block");
                $("#load_screen").css("display","none");
                $("#loading").css("display","none");            
            },
            error: function (request, status, error) {
                 $(".status-upload ").css("display","inline-block");
                 $("#load_screen").css("display","none");
                 $("#loading").css("display","none");  
            }
    });
}

var blog = {
    khoitao: function () {
        blog.dangkisukien();
    },
    dangkisukien: function () {   
       
        $(".pagination li a").off('click').on('click',function(){
            var data = $(this).data('role');
            var pading = 0;
            var pading_next = 0;
            if(data == 'left'){
                pading = Number($("#next_page").val());
                pading_next = pading+1;
            }
            if(data == 'right'){
                pading =Number($("#next_page").val());
                pading_next = pading -1;   
            }
            if(data=='content'){
                pading=Number($(this).text());
                pading_next = pading;
            }
            $("#next_page").val(pading_next);  
            console.log(pading_next);
            $( "#submit" ).trigger( "click" );
            blog.dangkisukien();
    });
    }
}
blog.khoitao();

