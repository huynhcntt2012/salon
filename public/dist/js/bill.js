Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#bill-page',
	/**
	 * service.list_cache is service cache in bill.
	 * service.list_service is service of serve.
	 * service.list_search_service is result search keyword.
	 */
	data: {
		service: {
			list_of_employee: [],
			list_cache:[],
			list_service: [],
			list_search_service: [],
			is_search: false,
			id_current: null,
			total_price: 0,
		},
		keyword: '',
		customer: {
			list_cache: [],
			keyword: '',
			list: [],
			is_search: false,
		},
		employee: {
			list_of_service: [],
			filterService: [],
			select_id: null,
			list_cache:[],
			keyword: '',
			list: [],
			is_search: false,	
		},
		bill: {
			discountRank: 0,
			discountMoney: 0,
			list: [],
			current_id: null,
			name_current: '',
			orders: [],
			customer: [],
			employee: [],
			list_cache: [],
			createDate: null,
			nameCurrent: ''
		},
		discount: {
			rank: true,
			money: false
		},
		points: [],
		rank_list: [],
		total_price: 0,
		formSubmitting: false,
		isNotSave: true,
	},
	ready: function(){
		this.getRank();
		this.getService();
		this.getBill();
		this.getPoint();
		this.getEmployee();
		// this.windowOnBeforeLoad();
		// this.windowOnLoad();
	},
	methods: {
		windowOnBeforeLoad: function() {
			var vm = this;
			window.onbeforeunload = function() {
				if (!vm.formSubmitting) {
					return "Do you really want to leave our brilliant application?";
				} else {
					return;
				}
			};
		},

		windowOnLoad: function () {
			var vm = this;
			window.onload = function() {
				window.addEventListener("beforeunload", function (e) {
				    if (vm.formSubmitting) {
				        return undefined;
				    }
				    // confirm('adadadasd','adasdasdas');
				    // alert('askjdhakjdhaksdas');
				    // (e || window.event).returnValue = confirmationMessage; //Gecko + IE
				    // return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
				    
				});
			};
		},

		test: function() {
			alert('asdhkajsdhakshdaksdhaksd');
		},

		getService: function() {
			this.$http.get(`api/service`)
				.then(function(response) {
					this.service.list_service = response.data;
				})
				.catch(error => {
					console.log(error);
				});
		},

		searchService: function() {
			var vm = this;
			if (this.keyword.replace(/\s+/g, ' ').length > 2) {
				this.service.is_search = true;
				this.$http.get(`api/service/search?q=${this.keyword}&offset=10&limit&10`)
					.then(function(response) {
						vm.service.list_search_service = response.data;
					})
					.catch(error=>{
						console.log(error);
					})
			} else {
				this.service.is_search = false;
			}
			Vue.set(this, 'service.is_search', this.service.is_search);
		},

		getCustomer: function() {
			var vm = this;
			if (this.customer.keyword.replace(/\s+/g, ' ').length > 1) {
				this.$http.get(`api/customer/search?q=${this.customer.keyword}&offset=0&limit&10`)
					.then(response => {
						vm.customer.list = response.data;
						for (var i = 0; i < vm.customer.list.length; i++) {
							for (var j = vm.rank_list.length - 1; j >= 0; j--) {
								if (vm.customer.list[i].point >= vm.rank_list[j].point) {
									vm.customer.list[i] = Object.assign(vm.customer.list[i], { rank: vm.rank_list[j].name});
									break;
								}
							}	
						}
					})
					.catch(error => {
						console.log(error);
					})
			} else {
				this.customer.list = [];
			}
			Vue.set(this, 'customer.list', this.customer.list);
		},

		getEmployee: function() {
			var vm = this;
			// if (this.employee.keyword.replace(/\s+/g, ' ').length > 1) {
				// this.$http.get(`api/employee/search?q=${this.employee.keyword}&offset=10&limit&10`)
				this.$http.get(`api/employeesAll`)
					.then(response => {
						vm.employee.list = response.data;
					})
					.catch(error => {
						console.log(error);
					})
			// } else {
			// 	this.employee.list = [];
			// }
			Vue.set(this, 'employee.list', this.employee.list);
		},

		getBill: function() {
			var vm = this;
			this.isNotSave = true;
			Vue.set(this, 'isNotSave', this.isNotSave);

			this.$http.get(`api/bill`)
				.then(response => {
					vm.bill.list = response.data;
					if (vm.bill.current_id) {
						setTimeout(function() {
							vm.chooseBill(vm.bill.current_id, vm.bill.name_current,true);
						}, 500);
					}
				})
				.catch(error => {
					console.log(error);
				})
		},

		getRank: function() {
			var vm = this;
			this.$http.get(`api/rank`)
				.then(response => {
					vm.rank_list = response.data;
				})
				.catch(error => {
					console.log(error);
				})
		},

		getPoint: function() {
			var vm = this;
			this.$http.get(`api/getpoint`)
				.then(response => {
					vm.points = response.data;
				})
				.catch(error => {
					console.log(error);
				})
		},

		createBill: function() {
			let vm = this;
			this.$http.post(`api/bill/create`)
				.then(response => {
					if (response.status == 201) {
						vm.getBill();
					}
				})
				.catch(error =>{
					console.log(error);
				});
		},

		chooseBill: function(id_bill, nameBill, isBrowser) {
			if (id_bill == this.bill.current_id && !isBrowser) {
				return false;
			}

			if (!isBrowser) {
				var nextBill = confirm('Bạn muốn rời khỏi đây','Xác nhận giúp tôi');
				if (!nextBill) {
					return false;
				}
			}


			this.isNotSave = false;
			Vue.set(this, 'isNotSave', this.isNotSave);

			let elementSelectBill = document.querySelector('.init-' + id_bill);
			if (this.bill.current_id) {
				let elementCurrentBill = document.querySelector('.init-' + this.bill.current_id);
				if (id_bill != this.bill.current_id) {
					try {
						elementCurrentBill.classList.remove('active');
						elementCurrentBill.classList.remove('box-comments');
					} catch(e) {}
					elementSelectBill.classList.add('active');
					elementSelectBill.classList.add('box-comments');
				}
			} 
			try {
				elementSelectBill.classList.add('active');
				elementSelectBill.classList.add('box-comments');
			} catch (error) {console.log(error);}


			this.bill.current_id = id_bill;
			this.bill.name_current = nameBill;
			this.fetchDataBillCurrent(this.bill.current_id);
			Vue.set(this,'bill.current_id',this.bill.current_id);
			Vue.set(this,'bill.name_current',this.bill.name_current);
		},

		/**
		 * fetchDataBillCurrent get detail of bill, and set data cache for service, customer and employee.
		 * @param  {[type]} id_bill is id of choose bill.
		 * @return {[type]}         [description]
		 */
		fetchDataBillCurrent: function(id_bill) {
			for (var i = 0; i < this.bill.list.length; i++) {
				if (this.bill.list[i].id == id_bill) {
					this.service.list_cache = this.bill.list[i].details;
					this.customer.list_cache = this.bill.list[i].customers;
					this.employee.list_cache = this.bill.list[i].employees;
					this.bill.discountRank = this.bill.list[i].discount_rank;
					this.bill.discountMoney = this.bill.list[i].discount_money;
					this.bill.createDate = this.bill.list[i].created_at;
					this.bill.nameCurrent =  this.bill.list[i].name;
					break;
				}
			}
			if (this.customer.list_cache.length > 0) {
				for (var i = this.rank_list.length - 1; i >= 0; i--) {
					if (this.customer.list_cache[0].point > this.rank_list[i].point) {
						this.customer.list_cache[0] = Object.assign(this.customer.list_cache[0],{ rank: this.rank_list[i].name });
						break;
					}
				}
			}

			if (this.bill.discountRank != null) {
				this.discount.rank =  true;
			} else {
				this.discount.rank =  false;
				this.bill.discountRank = 0
			}

			
			if (this.bill.discountMoney > 0) {
				this.discount.money = true;
			} else {
				this.discount.money = false;
				this.bill.discountMoney = 0
			}
			this.switchDiscount('money',this.discount.money);
			this.switchDiscount('rank',this.discount.rank);

			Vue.set(this, 'service.list_cache', this.service.list_cache);
			Vue.set(this, 'customer.list_cache', this.customer.list_cache);
			Vue.set(this, 'employee.list_cache', this.employee.list_cache);
			Vue.set(this, 'discountRank', this.discountRank);
			Vue.set(this, 'discountMoney', this.discountMoney);
			Vue.set(this, 'bill.createDate', this.bill.createDate);
			Vue.set(this, 'bill.nameCurrent', this.bill.nameCurrent);

			this.totalPriceOrder();
			if(this.customer.list_cache.length > 0) {
				this.calculatedDiscount();
			}
			if (this.totalPriceOrder() && this.calculatedDiscount()) {
				this.calculatedTotal();
			}
		},

		deleteBill: function(id_bill) {
			if (id_bill == null) {
				toastr.error('Chưa chọn hóa đơn', 'Lỗi!');
				return false;
			}
			let vm = this;
			this.$http.post(`api/bill/${id_bill}/delete`)
				.then(response => {
					vm.getBill();
					vm.bill.current_id = null;
					vm.bill.name_current = '';
				})
				.catch(error => {

				});
		},
		/**
		 * [addService] add service into bill.
		 * if - 1 is choose bill.
		 * if - 2 is compare id_parent with service get position service.
		 * if - 3 is compare id_child with sub get position sub in service.
		 * if - 4 is check service.list_cache exits or not to check duplicate or not service  when user choose.
		 * if - 5 is check service in list_cache exits or not.
		 * @param {[type]} id_parent [description]
		 * @param {[type]} id_child  [description]
		 * return array = [ 
		 * 					{	id: {[number]},
		 * 				  		name: {[string]},	
		 * 				    	price: {[number]},
		 * 				    	employees: [
		 * 				    			{	id: {[number]},
		 * 				    				firstname: {[string]},
		 * 				    				lastname: {[string]}	}
		 * 				    		]
		 * 				    }
		 * 			]
		 */
		addService: function(id_parent, id_child) {
			var is_duplicate = false,position_category = null, position_service = null;
			if (this.bill.current_id) {
				if (this.service.list_cache.length > 0) {
					// for (var i = 0; i < this.service.list_cache.length; i++) {
					// 	if (this.service.list_cache[i].id == id_child) {
					// 		is_duplicate = true;
					// 	}
					// }

					// if (is_duplicate) {
					// 	toastr.error('Đã tồn tại dịch vụ này trong hóa đơn','Lỗi');	
					// 	return false;
					// }
				}
				for (var i = 0; i < this.service.list_service.length; i++) {
					if (this.service.list_service[i].id == id_parent) {
						for (var j = 0; j < this.service.list_service[i].sub.length; j++) {
							if (this.service.list_service[i].sub[j].id == id_child) {
								position_category = i;
								position_service = j;
								var data = {
									id: this.service.list_service[i].sub[j].id,
									income: this.service.list_service[i].sub[j].income,
									name: this.service.list_service[i].sub[j].name,
									price: this.service.list_service[i].sub[j].price,
									employees: [],
								}
								this.service.list_cache = this.service.list_cache.concat(data);
							}
						}
					}
				}
			} else {
				toastr.warning('Chưa chọn hóa đơn');
				return false;
			}
		},

		/**
		 * [addCustomer] add cutomer into customer list_cache
		 * @param {[type]} id_customer [description]
		 */
		addCustomer: function(id_customer) {
			if (!this.bill.current_id) {
				toastr.warning('Chưa chọn hóa đơn');
				return false;	
			}
			if (!this.customer.list_cache.length >= 1) {
				for (var i = 0; i < this.customer.list.length; i++) {
					if (this.customer.list[i].id == id_customer) {
						this.customer.list_cache = this.customer.list_cache.concat(this.customer.list.slice(i,i+1));
					}
				}
			} else {
				toastr.error('Đã có khách hàng rồi', 'Lỗi!');
				return false;
			}
		},

		/**
		 * [removeFromListCache description]
		 * @param  {[type]} id [description]
		 * @return {[type]}  type: 1 - service
		 *                         2 - employee
		 *                         3 - customer	
		 */
		removeFromListCache: function(type,id) {
			if (type == 1) {
				for (var i = 0; i < this.service.list_cache.length; i++) {
					if (this.service.list_cache[i].id == id) {
						this.service.list_cache.splice(i,1);
						break;
					}
				}
			} else if (type == 2) {
				for (var i = 0; i < this.employee.list_cache.length; i++) {
					if (this.employee.list_cache[i].id == id) {
						this.employee.list_cache.splice(i,1);
						break;
					}
				}
			} else if (type == 3) {
				for (var i = 0; i < this.customer.list_cache.length; i++) {
					if (this.customer.list_cache[i].id == id) {
						this.customer.list_cache.splice(i,1);
						break;
					}
				}
			}
		},

		saveCacheBill: function(status) {
			var billId = this.bill.current_id;
			if (this.bill.current_id == null) {
				toastr.error('Chưa chọn hóa đơn', 'Lỗi!');
				return false;
			}
			var customer = [], data, vm = this;

			for (var i = 0; i < this.customer.list_cache.length; i++) {
				customer[i] = this.customer.list_cache[i].id;
			}
			if (status == 2) {
				if (customer.length < 1) {
					toastr.error('Chưa chọn khách hàng', 'Lỗi!');
					return false;
				}
				if (this.service.list_cache.length < 1) {
					toastr.error('Chưa chọn dịch vụ', 'Lỗi!');
					return false;
				}
				for (var i = 0; i < this.service.list_cache.length; i++) {
					if (this.service.list_cache[i].employees.length < 1) {
						toastr.error('Chưa chọn khách hàng cho dịch vụ', 'Lỗi!');
						return false;
					}
				}
			}
			data = {
				orders: this.service.list_cache,
				customer: customer,
				rank: this.discount.rank,
				money: this.discount.money,
				status: status
			}

			if (this.discount.rank) {
				data = Object.assign(data, {discountRank: parseFloat(this.bill.discountRank)});	
			}

			if (this.discount.money) {
				data = Object.assign(data, {discountMoney: parseFloat(this.bill.discountMoney)});	
			}

			if (status == 2) {
				var pointUse = Math.round(this.total_price * this.points.point / this.points.money);
				data = Object.assign(data, {pointUse: parseInt(pointUse)});
				this.bill.current_id = null;
				this.bill.name_current = '';
			}
			this.$http.post(`api/bill/${billId}/update`, data)
				.then(response => {
					if (response.status == 200) {
						toastr.success('Hoàn thành hóa đơn thành công', 'Ngon rồi!');
						vm.getBill();
						if (status == 2) {
							vm.resetBillListCache();
						}
						return true;
					}
				})
				.catch(error => {
					toastr.error(error.body, 'Lỗi!');
					return false;
				})
		},

		/**
		 * Show list employee on modal select employee.
		 * @param  {[type]} id_service [description]
		 * @return {[type]}            [description]
		 */
		getListEmployeeForService: function(id_service) {
			this.employee.list_of_service = [];
			for (var i = 0; i < this.service.list_cache.length; i++) {
				if (this.service.list_cache[i].id == id_service) {
					this.employee.list_of_service = this.service.list_cache[i].employees;
					Vue.set(this, 'employee.list_of_service', this.employee.list_of_service);
					return true;
				}
			}
		},

		/**
		 * Call modal list employee can add list_cache service and show employee exits in list_cache servie
		 * @param  {[type]} id_service [description]
		 * @return {[type]}            [description]
		 */
		callModalEmployee: function(id_service) {
			$('#employee-choose').modal('show');
			this.service.id_current = id_service;
			this.getListEmployeeForService(id_service);
			Vue.set(this,'service.id_current', this.service.id_current);
			// this.getEmployee();
			this.filterEmployee();
		},

		/**
		 * Filter employee not exist in list cache
		 * @return {[type]} [description]
		 */
		filterEmployee: function() {
			this.employee.filterService = [];
			for (var i = 0; i < this.employee.list.length; i++) {
				if (this.employee.list_of_service.length > 0) {
					let isExist = false;
					for (var j = 0; j < this.employee.list_of_service.length; j++) {
						if (this.employee.list_of_service[j].id == this.employee.list[i].id) {
							isExist = true;
						}
					}
					if (!isExist) {
						this.employee.filterService = this.employee.filterService.concat(this.employee.list[i]);
					}
				} else {
					this.employee.filterService = this.employee.list;
				}
			}
			Vue.set(this, 'employee.filterEmployee', this.employee.filterService);
		},

		/**
		 * Add employee into list_cache service.
		 * @param {[type]} id_service         [description]
		 * @param {[type]} id_employee        [description]
		 * @param {[type]} employee_firstname [description]
		 * @param {[type]} employee_lastname  [description]
		 */
		addEmployee: function(id_service, id_employee, employee_firstname, employee_lastname) {
			for (var i = 0; i < this.service.list_cache.length; i++) {
				if (this.service.list_cache[i].id == id_service) {
					for (var j = 0; j < this.service.list_cache[i].employees.length; j++) {
						if (this.service.list_cache[i].employees[j].id == id_employee) {
							toastr.error('Nhân viên này đã có rồi', 'Lỗi!');
							return false;
						}
					}

					var data = {
						id: id_employee,
						firstname: employee_firstname,
						lastname: employee_lastname,
					}
					this.service.list_cache[i].employees = this.service.list_cache[i].employees.concat(data);
					this.getListEmployeeForService(id_service);
				}
			}
			this.filterEmployee();
		},

		removeEmployee: function(id_service,id_employee) {
			for (var i = 0; i < this.service.list_cache.length; i++) {
				if (this.service.list_cache[i].id == id_service) {
					for (var j = 0; j < this.service.list_cache[i].employees.length; j++) {
						if (this.service.list_cache[i].employees[j].id == id_employee) {
							this.service.list_cache[i].employees.splice(j,1);
							this.getListEmployeeForService(id_service);
						}
					}
				}
			}
			this.filterEmployee();
		},

		totalPriceOrder: function() {
			this.service.total_price = 0;
			for (var i = 0; i < this.service.list_cache.length; i++) {
				this.service.total_price = parseInt(this.service.total_price) + parseInt(this.service.list_cache[i].price);
			}
			Vue.set(this, 'service.price', this.service.total_price);
			return true;
		},

		calculatedDiscount: function() {
			this.bill.discountRank = 0;
			this.total_price = 0;
			if (this.customer.list_cache.length <= 0) {
				return false;
			}

			for (var i = this.rank_list.length - 1; i >= 0; i--) {
				if (this.customer.list_cache[0].point > this.rank_list[i].point) {
					this.bill.discountRank = this.rank_list[i].discount;
					Vue.set(this,'bill.discountRank', this.bill.discountRank);
					break;
				}
			}
			return true;
		},

		calculatedTotal: function() {
			if (this.discount.rank && this.discount.money) {
				this.total_price = Math.round(this.service.total_price - (this.service.total_price*this.bill.discountRank)/100 - this.bill.discountMoney);
			} else if (this.discount.rank) {
				this.total_price = Math.round(this.service.total_price - (this.service.total_price*this.bill.discountRank)/100);
			} else if (this.discount.money) {
				this.total_price = Math.round(this.service.total_price - this.bill.discountMoney);
			} else {
				this.total_price = Math.round(this.service.total_price)
			}
			Vue.set(this, 'total_price', this.total_price);
		},

		resetBillListCache: function() {
			this.service.list_cache = [];
			this.customer.list_cache = [];
			this.employee.list_cache = [];
			this.discount.money =  false;
			this.discount.rank = false;
			this.bill.discountRank = 0;
			this.bill.discountMoney = 0;
			Vue.set(this, 'service.list_cache', this.service.list_cache);
			Vue.set(this, 'customer.list_cache', this.customer.list_cache);
			Vue.set(this, 'employee.list_cache', this.employee.list_cache);
		},

		switchDiscount: function(el, statusCheckBox, isWhere) {
			if (this.bill.current_id == null) {
				toastr.error('Chưa chọn hóa đơn', 'Lỗi!');
				return false;
			} 

			if (el == 'money') {
				this.discount.money = statusCheckBox;
				if (this.discount.money) {
					try {
						document.querySelector('.' + el  + '-discount').classList.remove('not-discount');
					} catch (e) {}
					if (isWhere == 1) {
						toastr.success('Chọn hình thức giảm gía theo hạng', 'Thành công');
					}
				} else {
					document.querySelector('.' + el  + '-discount').classList.add('not-discount');
					if (isWhere == 1) {
						toastr.warning('Bỏ chọn hình thức giảm gía theo hạng', 'Cảnh báo');
					}
				}
			} else if (el == 'rank') {
				this.discount.rank = statusCheckBox;
				if (this.discount.rank) {
					try {
						document.querySelector('.' + el  + '-discount').classList.remove('not-discount');
					} catch (e) {}
					if (isWhere == 1) {
						toastr.success('Chọn hình thức giảm gía theo số tiền', 'Thành công');
					}
				} else {
					document.querySelector('.' + el  + '-discount').classList.add('not-discount');
					if (isWhere == 1) {
						toastr.warning('Bỏ chọn hình thức giảm gía theo số tiền', 'Cảnh báo');
					}
				}
			}
			this.calculatedTotal();
		},

		printBill: function(isPrint) {
			if (!isPrint) {
				$('#modal-preview').modal('show');
			} else {
				// var printContents = document.getElementById(isPrint).innerHTML;
			 //    var originalContents = document.body.innerHTML;
			 //    document.body.innerHTML = printContents;
			    window.print();
			 //    document.body.innerHTML = originalContents;
			 //    var printContents = document.getElementById(isPrint).innerHTML;
				// var w = window.open();
				// w.document.write(printContents);
				// w.print();
				// w.close();
				// var noPrintEl = document.querySelectorAll('.no-print');
				// var printShow = document.querySelector('.print-show')
				// for (var i = 0; i < noPrintEl.length; i++) {
				// 	noPrintEl[i].style.display = 'none'
				// }
				// printShow.style.display = "block";
				// window.print();
				// for (var i = 0; i < noPrintEl.length; i++) {
				// 	noPrintEl[i].style.display = 'block'
				// }
				// printShow.style.display = 'none';
		    }
		},
	}
});