Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#contact',
	data: {
		contact: [],
		iscontact: false,
		add_phone: '',
		add_name: '',
        add_message: '',
        add_status: '0',
        contacts: {
			current_page: 1,
			total_page: null,
			last_page: null,
			total: null
		},
	},
	ready: function() {
		this.getContact();
        Vue.set(this, 'iscontact', this.iscontact);
	},
	methods: {
	   resetFrom: function(phone, name, message, status) {
			if (phone) {
				this.add_phone= phone;	
			} else {
				this.add_phone = '';
			}
            
            if (name) {
				this.add_name= name;	
			} else {
				this.add_name = '';
			}
            if (message) {
				this.add_message= message;	
			} else {
				this.add_message = '';
			}
            
            if (status) {
				this.add_status= status;	
			} else {
				this.add_status = '';
			}
			Vue.set(this, 'add_name', this.add_name);
			Vue.set(this, 'add_phone', this.add_phone);
            Vue.set(this, 'add_message', this.add_message);
			Vue.set(this, 'add_status', this.add_status);
		},
		getContact: function(page) {
		  var vmThis = this;
			if (!page) {
				page = 1;
			}
			this.$http.get(`api/getcontact?page=${page}`)
				.then(function(response) {
				vmThis.contact= {
						current_page: response.data.current_page,
						total_page: Math.ceil(response.data.total/response.data.per_page),
						last_page: response.data.last_page,
						total: response.data.total
					};
					Vue.set(vmThis, 'contact', vmThis.contact);
					Vue.set(this,'contacts',response.data.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		}
        
	}
})