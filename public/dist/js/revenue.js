Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#revenue',
	data: {
		dateFrom: '19-04-2017',
        dateTo: '19-04-2017',
        customerphone: [],
	},
	ready: function() {
	   //this.show();
	},
	methods: {
	   setDateNow: function() {
	        var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }else{
                today = new Date();
                if((today.getMonth()+1) < 10 )
                    month = '0' + (today.getMonth()+1);
                else
                    month = (today.getMonth()+1);
                
                date = today.getDate() +'-'+ month +'-'+ today.getFullYear();
                this.dateFrom = date;
                this.dateTo = date;
            }
            
		}
        ,
        pareDate: function(str){
            str = str.split('-');
            dateFormat = str[2]+str[1]+str[0];
            return dateFormat;
        }
	},
    watch: {
        dateTo: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateFrom = this.dateTo;
            }
        },
        dateFrom: function(val, oldVal) {
          // change of userinput, do something
            var vmThis = this;
            if(vmThis.pareDate(this.dateTo) < vmThis.pareDate(this.dateFrom)){
                this.dateTo = this.dateFrom;
            }
        }
  }
})