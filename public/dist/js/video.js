Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#video',
	data: {
		video: [],
		isvideo: false,
		add_link_youtube: '',
		add_title: '',
		add_creater: 'admin',
		is_edit: false,
		id_edit: null,
		video_id_selected: 0,
		video: {
			current_page: 1,
			total_page: 0,
			last_page: 0,
			total: 0
		},
        
		
	},
	ready: function() {
		this.getvideo();
		Vue.set(this, 'isvideo', this.isvideo);
	},
	methods: {
		/**
		 * Clear or get value for input in modal
		 * @param  {[type]} name     input name
		 * @param  {[type]} note     textare note
		 * @param  {[type]} price    input price
		 * @param  {[type]} income   input income
		 * @param  {[type]} selected select category
		 * @return {[type]} clear value for input or get value for input
		 */
		resetFrom: function(link, title, creater) {
			if (link) {
				this.add_link_youtube= link;	
			} else {
				this.add_link_youtube = '';
			}
            
            if (title) {
				this.add_title= title;	
			} else {
				this.add_title = '';
			}
            
            if (creater) {
				this.add_creater= creater;	
			} else {
				this.add_creater = 'admin';
			}
			Vue.set(this, 'add_link_youtube', this.add_link_youtube);
			Vue.set(this, 'add_title', this.add_title);
			Vue.set(this, 'add_creater', this.add_creater);
		},
		/**
		 * Get data for category
		 * @param  {[type]} page Get paginate wish go to
		 * @return {[type]}      Array data object
		 */
		getvideo: function(page) {
			var vmThis = this;
                        
			if (!page) {
				page = 1;
			}
			this.$http.get(`api/getvideo?page=${page}`)
				.then(function(response) {
				    vmThis.video= {
						current_page: response.data.current_page,
						total_page: Math.ceil(response.data.total/response.data.per_page),
						last_page: response.data.last_page,
						total: response.data.total
					};
                    Vue.set(vmThis, 'video1', vmThis.video);
					Vue.set(this,'video',response.data.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		
		/**
		 * Check current category or sub
		 * @param  {int} isVideo Number 1 is Category, 2 is Sub
		 * @return {[type]}            valiable is_video true/false
		 */
		checkService: function(isVideo) {
			this.is_edit = false;
			Vue.set(this, 'is_edit', this.is_edit);
			switch(isVideo) {
				case 1: 
					this.isvideo = false;
					break;
				case 2:
					this.isvideo =  true;
					break;
				default:
					this.isvideo = false;
					break;
			}
			Vue.set(this, 'isvideo', this.isvideo);
		},
		/**
		 * Create category or sub
		 * @param  {Boolean} isvideo valiable Gobal check wish create category or sub
		 * @return {[type]}            [description]
		 */
		createvideo: function() {
			if (this.add_title.replace(/\s+/g, ' ').length < 5) {
				toastr.error('Nhập tên phải trên 15 ký tự', 'Lỗi nha!');
				return false;
			}
            if (this.add_link_youtube.replace(/\s+/g, ' ').length < 5) {
				toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
				return false;
			}
			var vmThis = this, data;
			data = {
				title: this.add_title,
				link_youtube: this.add_link_youtube,
			}

			this.$http.post('api/createvideo', data)
				.then(function(response) {
					if (response.status == 200) {
						toastr.success('Tạo dịch vụ thành công', 'Thao tác thành công');
						$('#add-category').modal('hide');
						vmThis.resetFrom();
						vmThis.getvideo();
					}
				})
				.catch(function(error) {
					toastr.error('Thông báo kỹ thuật ngay 1', 'Lỗi chưa xác định');
					console.log(error);
				})
		},
		/**
		 * Delete Category by id
		 * @param  {[type]} id Is id of category.
		 * @return {[type]}    [description]
		 */
		deletevideo: function(id) {
			var vmThis = this;
			this.$http.delete(`api/delvideo/${id}`)
				.then(function(response) {
					if (response.status == 200) {
						vmThis.getvideo();
						toastr.success('Xóa thành công', 'Thao tác thành công');
					}
				})
				.catch(function(error) {
					console.log(error);
				})
		},
		/**
		 * Update Category or sub
		 * @param  {Boolean} isService Check update category or update sub
		 * @return {[type]}            [description]
		 */
		editvideo: function(isService) {
			if (this.add_title.replace(/\s+/g, ' ').length < 5) {
				toastr.error('Nhập tên phải trên 15 ký tự', 'Lỗi nha!');
				return false;
			}
            if (this.add_link_youtube.replace(/\s+/g, ' ').length < 5) {
				toastr.error('Nhập tên phải trên 5 ký tự', 'Lỗi nha!');
				return false;
			}
			var vmThis = this, data;
			data = {
				title: this.add_title,
				link_youtube: this.add_link_youtube,
			}

    		this.$http.put(`api/editvideo/${this.id_edit}`, data)
    			.then(function(response) {
    				if (response.status == 200) {
    					toastr.success('Cập nhật dịch vụ thành công', 'Thao tác thành công');
    					$('#add-category').modal('hide');
    					vmThis.resetFrom();
    					vmThis.getvideo();
    				}
    			})
    			.catch(function(error) {
    				toastr.error('Thông báo kỹ thuật ngay 1', 'Lỗi chưa xác định');
    				console.log(error);
    			})
				
			
		},
		
		getSingleSub: function(id) {
			this.checkService(2);
			this.is_edit = true;
			this.id_edit = id;
			Vue.set(this, 'is_edit', this.is_edit);
			for (var i = this.video.length - 1; i >= 0; i--) {
				if (this.video[i].id == id) {
					this.resetFrom(
						this.video[i].link_youtube, 
						this.video[i].title
						);
				}
			}
		},
		
		/**
		 * Switch get all sub of get sub by category.
		 * @param  {[type]} page is wish go to.
		 * @return {[type]}      Array object
		 */
		switchPage: function(page) {
			this.getvideo(page);
		}
	}
})