<li class="header">MAIN NAVIGATION</li>
        
        <li <?php if($selectedmenu == 1 && $selectedmenu == 1) echo 'class="active"'; ?>><a href="{{ route('bill') }}"><i class="fa fa-book"></i> <span>Phiếu Dịch Vụ</span></a></li>
        <li class="<?php if($selectedmenu == 2) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Khách Hàng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 2 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('customerinfo') }}"><i class="fa fa-circle-o"></i> Tra Cứu Thông Tin</a></li>
            <li <?php if($selectedmenu == 2 && $selecteditem == 2) echo 'class="active"'; ?>><a href="{{ route('customerdate') }}"><i class="fa fa-circle-o"></i> Tra Cứu Theo Ngày</a></li>
            <li <?php if($selectedmenu == 2 && $selecteditem == 3) echo 'class="active"'; ?>><a href="{{ route('customer') }}"><i class="fa fa-circle-o"></i> Thêm Khách Hàng</a></li>
          </ul>
        </li>
        
        <li class="<?php if($selectedmenu == 3) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Bảng Tin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            
            <li <?php if($selectedmenu == 3 && $selecteditem == 2) echo 'class="active"'; ?>><a href="{{ asset('admin/homeBlog') }}"><i class="fa fa-circle-o"></i> Bảng Tin Salon</a></li>
            
          </ul>
        </li>
        <li>
        
        <li class="<?php if($selectedmenu == 4) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Nhân Viên</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 4 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('employeetour') }}"><i class="fa fa-circle-o"></i>Hoa Hồng Dịch Vụ</a></li>
            <li <?php if($selectedmenu == 4 && $selecteditem == 2) echo 'class="active"'; ?>><a href="{{ route('employee') }}"><i class="fa fa-circle-o"></i>Danh Sách Nhân Viên</a></li>
          </ul>
        </li>
        <li>
        
        <li class="<?php if($selectedmenu == 5) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Dịch Vụ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 5 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('servicecategory') }}"><i class="fa fa-circle-o"></i> Danh Mục</a></li>
          </ul>
        </li>
        
        <li <?php if($selectedmenu == 6 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('image') }}"><i class="fa fa-book"></i> <span>Kho Ảnh</span></a></li>
        
        <li <?php if($selectedmenu == 7 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('video') }}"><i class="fa fa-book"></i> <span>Kho Video</span></a></li>
        
        <li class="<?php if($selectedmenu == 8) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Chăm Sóc Khách Hàng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 8 && $selecteditem == 1) echo 'class="active"'; ?>><a href="{{ route('point') }}"><i class="fa fa-circle-o"></i> Cách Đổi Điểm</a></li>
            <li <?php if($selectedmenu == 8 && $selecteditem == 2) echo 'class="active"'; ?>><a href="{{ route('contact') }}"><i class="fa fa-circle-o"></i> Phản Hồi Khách Hàng</a></li>
          </ul>
        </li>
        
        <li class="<?php if($selectedmenu == 9) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Thống Kê</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 9 && $selecteditem == 1) echo 'class="active"' ?>><a href="{{ route('revenue') }}"><i class="fa fa-circle-o"></i> Doanh Thu</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 2) echo 'class="active"' ?>><a href="{{ route('salaries') }}"><i class="fa fa-circle-o"></i> Lương Nhân Viên</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 3) echo 'class="active"' ?>><a href="{{ route('revenexpen') }}"><i class="fa fa-circle-o"></i> Các Khoản Thu Chi</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 4) echo 'class="active"' ?>><a href="{{ route('customersta') }}"><i class="fa fa-circle-o"></i> Khách Hàng</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 5) echo 'class="active"' ?>><a href="{{ route('servicessta') }}"><i class="fa fa-circle-o"></i> Dịch Vụ</a></li>
          </ul>
        </li>
        
        <li class="<?php if($selectedmenu == 10) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Thiết Bị</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 10 && $selecteditem == 1) echo 'class="active"' ?>><a href="{{ route('device') }}"><i class="fa fa-circle-o"></i>Điện Thoại</a></li>
            </ul>
        </li>
        <li class="<?php if($selectedmenu == 11) echo 'active'; ?> treeview">
          <a href="{{ route('stock.goods') }}">
            <i class="fa fa-dashboard"></i> <span>Phiếu kho</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="<?php if($selectedmenu == 12) echo 'active'; ?> treeview">
          <a href="{{ route('stock.voucher') }}">
            <i class="fa fa-dashboard"></i> <span>Phiếu thu chi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="<?php if($selectedmenu == 13) echo 'active'; ?> treeview">
          <a href="{{ route('stock.provider') }}">
            <i class="fa fa-dashboard"></i> <span>Nhà cung cấp</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>