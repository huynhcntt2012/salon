@extends('admin.main')

@section('css')
<style type="text/css">
	.mr-right-10 {
		margin-right: 10px;
	}
	.pd-right-no {
		padding-right: 0px;
	}
	.cursor-p {
		cursor:  pointer;
	}
	.box .nav-stacked > li {
		border-bottom: 3px solid #e6e2e2;
	}
	.not-discount > .text-discount {
		text-decoration: line-through; 
		color: #999
	}
	.bd-bt {
		border-bottom:  1px solid #f4f4f4;
	}
</style>
<link  rel="stylesheet" href="{{asset('/public/dist/css/print-bill.css')}}">
@endsection
@section('content')
	<section class="content" id="bill-page">
		<div class="row no-print">
			<div class="col-md-3 pd-right-no">
				<a href="javascript:void(0)" class="btn btn-primary btn-block margin-bottom" @click="createBill()">Tạo hóa đơn</a>
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Hóa đơn tạm</h3>
					</div>
					<div class="box-body no-padding">
						<ul class="nav nav-stacked" id="bill-box">
							<li v-for="bill in bill.list" class="init-@{{ bill.id }}" @click="chooseBill(bill.id,bill.name,isNotSave)">
								<a href="javascript:void(0)">
									<i class="fa fa-inbox"></i> @{{ bill.name }}
										<i v-if="bill.customers.length < 1" data-toggle="tooltip" data-original-title="Thiếu khách hàng" class="icon fa fa-users text-yellow pull-right"></i>
										<i v-if="bill.employees.length < 1" data-toggle="tooltip" data-original-title="Thiếu nhân viên" class="fa fa-child text-blue pull-right"></i>
										<i v-if="bill.details.length < 1" data-toggle="tooltip" data-original-title="Thiếu dịch vụ" class="fa fa-shopping-cart text-red pull-right"></i>
									<br>
									<small class="text-muted">
										<i class="fa fa-clock-o"></i> @{{ bill.created_at | moment 'DD/MM/YYYY'  }}
									</small>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-6 pd-right-no">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Chi tiết đơn hàng: @{{ bill.name_current }}</h3>
					</div>
					<div class="box-body">
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#service" data-toggle="tab">
										<i class="fa fa-shopping-cart text-red mr-right-10"></i>Dịch vụ
									</a>
								</li>
								<li>
									<a href="#customer" data-toggle="tab">
										<i class="fa fa-users text-yellow mr-right-10"></i> Khách hàng
									</a>
								</li>
								{{-- <li>
									<a href="#employee" data-toggle="tab">
										<i class="fa fa-child text-blue mr-right-10"></i>Nhân viên
									</a>
								</li> --}}
							</ul>
							<div class="tab-content" id="bill-box-detail">
								<div class="active tab-pane" id="service">
									<ul class="nav nav-stacked">
										<li class="post cursor-p" v-for="service in service.list_cache" @click="callModalEmployee(service.id)">
											<div class="user-block" >
												<span class="username">
													<a href="javascript:void(0)">@{{ service.name }}</a>
													<span class="label label-warning">@{{ service.price | number }} VND</span></a>
													<a href="javascript:void(0)" 
														class="pull-right margin btn-box-tool"
														@click="removeFromListCache(1,service.id)" 
													><i class="fa fa-times"></i></a>
												</span>
												<span class="description">@{{ service.price }}</span>
												<ul>
													<li v-for="employee in service.employees"><small class="label label-default">@{{ employee.firstname }} @{{ employee.lastname }}</small></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>

								<div class="tab-pane" id="customer">
									<div class="post" v-for="customer in customer.list_cache">
										<div class="user-block">
											<span class="username">
												<a href="#">@{{ customer.name }}</a>
												<small class="label label-info" v-if="customer.rank">@{{ customer.rank }}</small>
												<a href="javascript:void(0)" 
													class="pull-right btn-box-tool margin"
													{{-- data-toggle="tooltip" 
													data-original-title="Xóa" --}}
													@click="removeFromListCache(3,customer.id)"
												><i class="fa fa-times"></i></a>
											</span>
											<span class="description">@{{ customer.phone }}</span>
										</div>
									</div>
									<div class="post">
										<div class="user-block">
											<div class="form-group">
												<div class="col-sm-12">
													<div class="has-feedback">
														<input type="text" 
															class="form-control input-sm" 
															placeholder="Tìm khách hàng"
															v-model="customer.keyword"
															v-on:keyup="getCustomer()">
														<span class="glyphicon glyphicon-search form-control-feedback text-muted"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="post" v-for="customer in customer.list" >
										<div class="user-block">
											<span class="username">
												<a href="javascript:void(0)">@{{ customer.name }}</a>
												<small class="label label-info" v-if="customer.rank" >@{{ customer.rank }}</small>
												<button type="button" class="btn bg-olive margin pull-right" @click="addCustomer(customer.id)">Thêm vào đơn hàng</button>
											</span>
											<span class="description">@{{ customer.birthday | moment 'DD/MM/YYYY' }} - @{{ customer.phone }}</span>
										</div>
									</div>
									<div class="post" v-if="customer.list.length == 0">
										<span>Không có tên khách hàng cần tìm!!!</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-12 form-horizontal">
								<div class="form-group">
									<label for="total-pirce" class="col-sm-4 control-label">Tổng cộng:</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="total-pirce" v-model="service.total_price | number" disabled>
									</div>
									<label class="col-sm-1 control-label">VND</label>
								</div>
								<div class="form-group rank-discount" v-bind:class="{ 'not-discount': !discount.rank}">
									<label for="discount" class="col-sm-4 control-label text-discount">Giảm gía theo hạng:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" id="discount"  v-model="bill.discountRank" disabled="disabled" >
									</div>
									<label class="col-sm-1 control-label text-discount">%</label>
									<div class="col-sm-2  control-label">
										<input type="checkbox" v-model="discount.rank" @click="switchDiscount('rank',!discount.rank,1)">
									</div>
								</div>
								<div class="form-group money-discount" v-bind:class="{ 'not-discount': !discount.money}">
									<label for="discount-money" class="col-sm-4 control-label text-discount">Giảm gía tiền mặt:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" id="discount-money"  v-model="bill.discountMoney">
									</div>
									<label class="col-sm-1 control-label text-discount">VND</label>
									<div class="col-sm-2  control-label">
										<input type="checkbox" v-model="discount.money" @click="switchDiscount('money',!discount.money,1)">
									</div>
								</div>
								<div class="form-group">
									<label for="total-pirce" class="col-sm-4 control-label">Tổng tiền:</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="total-pirce" v-model="total_price | number" disabled>
									</div>
									<label class="col-sm-1 control-label">VND</label>
								</div>
								<button type="button" class="btn bnt-info btn-flat col-sm-3" @click="saveCacheBill(1)"><i class="fa fa-save"></i>&nbsp;&nbsp;Lưu tạm</button>
								<button type="button" class="btn bg-olive btn-flat col-sm-3" @click="saveCacheBill(2)"><i class="fa fa-check"></i>&nbsp;&nbsp;Hoàn thành</button>
								<button type="button" class="btn btn-primary btn-flat col-sm-3" @click="printBill()"><i class="fa fa-print"></i>&nbsp;&nbsp;In hóa đơn</button>
								<button type="button" class="btn btn-danger btn-flat col-sm-3" @click="deleteBill(bill.current_id)"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Xóa</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3 pd-right-no">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Dịch vụ hệ thống</h3>
					</div>
					<div class="box-body" id="service-system">
						<div class="form-group has-feedback">
							<input type="text" class="form-control input-sm" v-model="keyword" placeholder="Search..." v-on:keyup="searchService()">
							<span class="glyphicon glyphicon-search form-control-feedback text-muted"></span>
						</div>
						<div v-if="!service.is_search" >
							<div class="box box-solid box-success collapsed-box" 
								v-for="service in service.list_service" 
							>
								<div class="box-header with-border">
									<h3 class="box-title">@{{ service.name }}</h3>

									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
									</div>
								</div>
								<div class="box-body">
									<ul class="products-list product-list-in-box" v-if="service.sub.length > 0">
										<li class="item" v-for="sub in service.sub" v-if="service.sub.length > 0" >
											<a href="javascript:void(0)" class="product-title">@{{ sub.name }}<span class="label label-warning">@{{ sub.price | number }} VND</span></a>
											<a href="javascript:void(0)" 
												class="pull-right" 
												data-toggle="tooltip" 
												data-original-title="Thêm vào hóa đơn" 
												@click="addService(service.id,sub.id)"
												><span class="label label-primary"><i class="fa fa-cart-plus"></i></span></a>
										</li>
									</ul>
									<ul class="products-list product-list-in-box" v-if="service.sub.length == 0">
										<span>Hiện chưa có dịch vụ</span>
									</ul>
								</div>
							</div>
						</div>
						<ul class="products-list product-list-in-box" v-if="service.is_search">
							<li class="item" v-for="service in service.list_search_service">
								<div class="product-title">
									<a href="javascript:void(0)" class="product-title">@{{ service.name }}
									<span class="label label-warning">@{{ service.price | number }} VND</span></a>
									<a href="javascript:void(0)" 
											class="pull-right" 
											data-toggle="tooltip" 
											data-original-title="Thêm vào hóa đơn" 
											@click="addService(service.category,service.id)"
											><span class="label label-primary"><i class="fa fa-cart-plus"></i></span></a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade no-print" id="setting-service" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Dịch vụ hiện có trong hóa đơn </h4>
						<span class="text-muted">Lưu ý: nhân viên sẽ làm những dịch hiện có trong hóa đơn.</span>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<button v-for="service in service.list_cache" class="btn bg-default margin btn-reset init-1" @click="chooseServiceForEmployee($event,service.id,service.name)">@{{ service.name }}</button>
							</div>	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
						<button v-if="!is_edit" type="button" class="btn btn-primary" @click="addEmployee(employee.select_id)">Thêm</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade no-print" id="employee-choose" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Thêm nhân viên cho dịch vụ </h4>
						<span class="text-muted">Lưu ý: nhân viên được chọn sẽ làm cho dịch vụ này.</span>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<ul class="nav nav-stacked" id="modal-service-current">
										<li v-for="employee in employee.list_of_service" >
											<a href="javascript:void(0)">@{{ employee.firstname }} @{{employee.lastname}}
												<span class="pull-right badge bg-red"><i class="fa fa-minus" @click="removeEmployee(service.id_current,employee.id)"></i></span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box-footer no-padding">
									<div class="form-group">
										<ul class="nav nav-stacked" id="modal-service">
											<template  v-for="employeeServer in employee.filterService" >
												<li><a href="javascript:void(0)"> @{{ employeeServer.firstname }} @{{ employeeServer.lastname }}<span class="pull-right badge bg-blue"><i  @click="addEmployee(service.id_current,employeeServer.id,employeeServer.firstname,employeeServer.lastname)" class="fa fa-plus"></i></span></a></li>
											</template>
										</ul>
									</div>
								</div>
							</div>	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Xong</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade no-print" id="modal-preview" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Hệ thống dịch vụ salon</h4>
						<span class="text-muted">Lưu ý: xem lại trước khi in.</span>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="form-group" id="preview-bill">
								<div class="col-md-12 form-group" style="text-align: center;">
									<h4 class="modal-title">Hệ thống dịch vụ salon</h4>
								</div>	
								<div class="col-md-12">
									<span class="col-sm-5">Mã hóa đơn:</span>
									<span class="col-sm-7">@{{ bill.nameCurrent }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Ngày:</span>
									<span class="col-sm-7">@{{ bill.createDate }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Tên khách hàng:</span>
									<span class="col-sm-7">@{{ customer.list_cache[0].name }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Số điện thoại:</span>
									<span class="col-sm-7">@{{ customer.list_cache[0].phone }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Thông tin chi tiết hóa đơn:</span>
									<span class="col-sm-7"></span>
								</div>
								<div class="col-md-12 bd-bt">
									<table class="table">
										<tbody>
											<tr>
												<th style="width: 10px">STT</th>
												<th>Dịch vụ</th>
												<th>Nhân viên</th>
												<th style="width: 120px;">Gía</th>
											</tr>
											<tr v-for="service in service.list_cache">
												<td>@{{ $index + 1 }}.</td>
												<td>@{{ service.name }}</td>
												<td>
													<template v-for="employee in service.employees">
														<span>@{{ employee.firstname }} @{{ employee.lastname }}</span>
														<br>
													</template>
												</td>
												<td><span>@{{ service.price | number }}</span></td>
											</tr>
										</tbody>
									</table>
								</div>	
								<div class="col-md-12">
									<span class="col-sm-5">Tổng tiền:</span>
									<span class="col-sm-7">@{{ service.total_price | number }} VND</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Giảm gía</span>
									<span class="col-sm-7" v-if="discount.rank">@{{ bill.discountRank }} %</span>
									<span class="col-sm-7" v-if="discount.money">@{{ bill.discountMoney | number }} VND</span>
									<span class="col-sm-7" v-if="!discount.rank && !discount.money">0</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Thành tiền:</span>
									<span class="col-sm-7">@{{ total_price | number }} VND</span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
						<button type="button" class="btn btn-primary" @click="printBill('preview-bill')">Tiếp tục in</button>
					</div>
				</div>
			</div>
		</div>

		<div id="print-preview" class="print-show">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body print-show">
						<div class="row ">
							<div class="form-group">
								<div class="col-md-12 form-group" style="text-align: center;">
									<h4 class="modal-title">Hệ thống dịch vụ salon</h4>
								</div>	
								<div class="col-md-12">
									<span class="col-sm-5">Mã hóa đơn:</span>
									<span class="col-sm-7">@{{ bill.nameCurrent }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Ngày:</span>
									<span class="col-sm-7">@{{ bill.createDate }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Tên khách hàng:</span>
									<span class="col-sm-7">@{{ customer.list_cache[0].name }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Số điện thoại:</span>
									<span class="col-sm-7">@{{ customer.list_cache[0].phone }}</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Thông tin chi tiết hóa đơn:</span>
									<span class="col-sm-7"></span>
								</div>
								<div class="col-md-12 bd-bt">
									<table class="table">
										<tbody>
											<tr>
												<th style="width: 10px">STT</th>
												<th>Dịch vụ</th>
												<th>Nhân viên</th>
												<th style="width: 120px;">Gía</th>
											</tr>
											<tr v-for="service in service.list_cache">
												<td>@{{ $index + 1 }}.</td>
												<td>@{{ service.name }}</td>
												<td>
													<template v-for="employee in service.employees">
														<span>@{{ employee.firstname }} @{{ employee.lastname }}</span>
														<br>
													</template>
												</td>
												<td><span>@{{ service.price | number }}</span></td>
											</tr>
										</tbody>
									</table>
								</div>	
								<div class="col-md-12">
									<span class="col-sm-5">Tổng tiền:</span>
									<span class="col-sm-7">@{{ service.total_price | number }} VND</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Giảm gía</span>
									<span class="col-sm-7" v-if="discount.rank">@{{ bill.discountRank }} %</span>
									<span class="col-sm-7" v-if="discount.money">@{{ bill.discountMoney | number }} VND</span>
									<span class="col-sm-7" v-if="!discount.rank && !discount.money">0</span>
								</div>
								<div class="col-md-12">
									<span class="col-sm-5">Thành tiền:</span>
									<span class="col-sm-7">@{{ total_price | number }} VND</span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left no-print" data-dismiss="modal">Hủy</button>
						<button type="button" class="btn btn-primary no-print" onclick="window.print()">Tiếp tục in</button>
						{{-- printBill('print-preview') --}}
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('/public/dist/js/bill.js')}}"></script>
<script>
	$('#bill-box').slimScroll({
		height: '550px'
	});
	$('#bill-box-detail').slimScroll({
		height: '303px'
	});
	$('#service-system').slimScroll({
		height: '600px'
	});
	$('#modal-service').slimScroll({
		heìght: '200px'
	})

</script>
@endsection
