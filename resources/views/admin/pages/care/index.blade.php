@extends('admin.main')
@section('css')
    <link href="../public/asset/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="../public/asset/css/typeahead.css" rel="stylesheet">
    <link href="../public/asset/css/checkout.css" rel="stylesheet">
    <link href="../public/asset/css/report.css" rel="stylesheet">
    <link href="../public/asset/css/common.css" rel="stylesheet">
@stop
@section('js')
	<script src="{{asset('/public/dist/js/contact.js')}}"></script>
@endsection
@section('content')

<form method="post" data-action="" id="frm-reload-data">
<section id="contact">
<div id="page-wrapper">  
        <div class="row">
        <div class="span12 columns">
        <div class="dataTables_wrapper no-footer" data-type="limit-offset">
        <div class="row"> 
            <div class="page-size col-md-6 dataTables_filter" align="right">
               <div id="search-order">
                    <label>Tìm kiếm:<input type="search" class="data-search-input" aria-controls="" name="keyword" value=""></label>
              </div>               
            </div>
        </div>
          <table id="customer_table" class="table table-hover table-bordered table-responsive table-striped" data-type="limit-offset">
            <thead>
              <tr>
                <th class="header" style="width:5%;">STT</th>          
                <th class="header" style="width:20%;">Tên khách hàng</th>
                <th class="header">Nội dung</th>
                <th class="header" style="width:12%;">Ngày gởi</th>
                <th class="header" style="width:5%;">Đã xem</th>
                <th class="header" style="width:5%;">Xóa</th>                
              </tr>
            </thead>
            <tbody>
            <tr v-for="contact in contacts">
                <td class="header" style="width:5%;">@{{ $index + 1 }}</td>          
                <td class="header" style="width:20%;"> @{{ contact.phone }}</td>
                <td class="header">@{{ contact.message }}</td>
                <td class="header" style="width:12%;">@{{ contact.created_at }}</td>
                <td class="header" style="width:5%;">Đã xem</td>
                <td class="header" style="width:5%;">Xóa</td>                
              </tr>  
            </tbody>
          </table>
          <div class="box-footer clearfix no-border">
						<div class="box-tools pull-right">
							<ul class="pagination pagination-sm inline">
								<li @click="getContact(1)"><a>&laquo;</a></li>
								<li v-for="page in contact.total_page"  
									@click="getContact(page + 1)"
									:class="((page + 1) == category.current_page) ? 'active' : ''" 
									><a>@{{ page + 1 }}</a></li>
								<li @click="getContact(category.last_page)" ><a>&raquo;</a></li>
							</ul>
						</div>
					</div> 
        </div> 
           
      </div>
</div><!-- /#page-wrapper -->       

    

  
    </div>
    </section>
    </form>
    
        
@stop


