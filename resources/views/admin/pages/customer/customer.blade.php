
@extends('admin.main')
@section('css')
   <style>
.container{
    margin-top:20px;
}
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>
   
@stop

@section('content')
    
<div id="customer" class="box-body">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tra Cứu Thông Tin</h3>
                <form method="POST" enctype="multipart/form-data" action="customer" >
                    <div class="form-group">
                        <div style="width: 40%; float: left;">
                            <label for="title">Tên Khách Hàng</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="name">
                        </div>
                        <div class="clear"></div>
                        <div style="width: 40%; float: right;">
                            <label for="title">Số Điện Thoại</label>
                            <input type="text"  class="form-control" id="phone" name="phone" placeholder="phone">
                        </div>
                    <div class="clear"></div>
                    </div>
                    
                    <div class="form-group">
                        <div class="box-footer">
                            <div style="padding-top: 30px;" class="col-md-12">
                                <div class="pull-right">
                                    <button  @click="getSearch()"  type="button" class="btn btn-primary"><i></i> Tìm Kiếm</button>
                                </div>
                        </div>
                    </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
       <section class="content">
      <div class="row">
        <div id="add-category" role="dialog" class="col-md-12">
        <div class="box">
            <div class="box-header">
            <table class="table table-striped">
							<thead>
								<tr>
									<th class="col-sm-2">STT</th>
									<th class="col-sm-7">Tên </th>
                                    <th class="col-sm-7">Số điện thoại </th>
									<th class="col-sm-3">Chức năng</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="cus in customerlook">
									<td class="col-sm-2">@{{ $index + 1 }}</td>
									<td class="col-sm-7">@{{ cus.name }}</td>
                                    <td class="col-sm-7">@{{ cus.phone }}</td>
									<td class="col-sm-3">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cogs"></i>
											</button>
											<ul class="dropdown-menu pull-right" role="menu">
												<li><a data-toggle="modal" data-target="#add-category" @click="getdataCategory(category.id)" >Chỉnh sửa</a></li>
												<li><a href="javascript:void(0)" @click="deleteCategory(category.id)">Xóa</a></li>
											</ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
          </div>
        </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    </section>
    </div>
@stop

@section('js')
	<script src="{{asset('/public/dist/js/customer.js')}}"></script>
@endsection



