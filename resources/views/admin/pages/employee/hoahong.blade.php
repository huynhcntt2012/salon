@extends('admin.main')

@section('css')

@endsection

@section('content')
<section id="employee" class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                	<div class="form-group">
                        <label style="width: 100%;float: left;" for="title">Nhân Viên</label>
                        <input type="text" style="width: 30%;float: left;" v-model="add_id" class="form-control" id="cmnd" name="CMND" placeholder="CMND" value="123456">
                        <button  type="button" @click="getemployee()" data-toggle="modal" data-target="#add-category" class="search-form__submit btn btn-primary">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                    <div v-for="employee in employee" class="box-header">
                    	<div class="form-group"></div>
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Tên Nhân Viên</label>
                            <input type="text" style="width: 50%;float: left;" class="form-control" id="name" name="name" placeholder="tên nhân viên" value="@{{employee.firstname}}">
                        </div>
                            <input type="hidden" hidden="hidden" style="width: 50%;float: left;" v-model="add_idperson" class="form-control" id="id" name="id" placeholder="tên nhân viên" value="@{{employee.id}}">
                        
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Số Điện Thoại</label>
                            <input type="text" style="width: 50%;float: left;" class="form-control" id="phone" name="phone" placeholder="số điện thoại" value="@{{employee.phone}}">
                        </div>
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Ngày Sinh</label>
                            <input type="date" style="width: 50%;float: left;" class="form-control" id="birthday" name="birthday" placeholder="ngày sinh" value="@{{employee.birthday}}">
                        </div>
                        
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Địa Chỉ</label>
                            <input type="text" style="width: 50%;float: left;" class="form-control" id="address" name="address" placeholder="địa chỉ" value="@{{employee.address}}">
                        </div>
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Lương</label>
                            <input type="text" style="width: 50%;float: left;" class="form-control" id="salary" name="salary" placeholder="lương" value="@{{employee.salary}}">
                        </div>
                        <div class="form-group">
                            <label style="width: 50%;float: left;" for="title">Nhập lại mật khẩu</label>
                            <input type="text" style="width: 50%;float: left;" class="form-control" id="password" name="password" placeholder="mật khẩu" value="@{{employee.password}}">
                        </div>
                        <div class="box-footer">
                            <div class="pull-right">
                            <button style="float: right;margin-top: 10px;" type="submit" class="btn btn-primary"><i></i> Lưu</button>
                          </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                <h3>Dịch Vụ</h3>
                <table id="customer_table" class="table table-hover table-bordered table-responsive table-striped" data-type="limit-offset">
                    <thead>
                      <tr>
                        <th class="header" style="width:5%;">STT</th>          
                        <th class="header" style="width:20%;">Danh Mục</th>
                        <th class="header">Tên Dịch Vụ</th>
                        <th class="header" style="width:16%;">Tiền mặt</th>
                        <th class="header" style="width:16%;">Phần Trăm</th>
                        <th class="header" style="width:5%;">Tình Trạng</th>                
                      </tr>
                    </thead>
                    <tbody>
                        <tr v-for="sub in subs">
                        	<td class="header" style="width:5%;">@{{$index +1}}</td>          
                        	<td class="header" style="width:20%;">@{{ sub.name1 }}</td>
                        	<td class="header">@{{ sub.name }}</td>
                        	<td class="header" style="width:16%;"><input name="income@{{sub.idService}}" id="income@{{sub.idService}}" type="number" value="@{{ sub.income }}" /></td>
                        	<td class="header" style="width:16%;"><input name="percent@{{sub.idService}}" id="percent@{{sub.idService}}" type="number" value="@{{ sub.percent }}" /></td>
                        	<td class="header" style="width:5%;"><button @click="saveIncome(sub.idService,sub.idService)" class="btn btn-primary">Lưu</button></td>                
                        </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
	<script src="{{asset('/public/dist/js/hoahong.js')}}"></script>
@endsection
