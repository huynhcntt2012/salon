@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>
@stop
@section('content')
<h2>Kho Ảnh <a class="btn btn-success" href="uploadimage">Upload Ảnh</a></h2>
<form id="frm_search" action="{{url('admin/image')}}" method="Get" class="form-inline" style="padding-right:5px;margin-bottom: 10px;">
        <div class="form-group">
          <label for="email">Tiêu đề</label>
          <input type="text" class="form-control" name="keyword" value="{{$keyword}}"  id="keyword">
        </div>
    <button type="submit" id="submit" class="btn btn-default">Tìm kiếm</button>
  </form>
        
    <table class="table table-blog" style="background-color: #fff ">
      <thead>
        <tr>
          <th>
              Stt         
          </th>
          <th>
              Tiêu đề            
          </th>
          <th>Hình ảnh</th>
          <th>Thể Loại</th>
          <th>Người tạo</th>
          <th>Tùy Chọn</th>
        </tr>
      </thead>
      <tbody>

        <?php $stt = 1;?>

        @foreach ($data as $data )
          <tr>
            <td>{{ $stt++ }}</td>
            <td> {{ $data->title }}</td>
            <td><img style="max-width: 100px; max-height: 100px;" src="{{ asset('public/images/'.$data->name.'') }}"></td>
            @foreach($data->type as $type)
            <td>{{ $type->name }}</td>
            @endforeach
            <td>{{ $data->creater }}</td>
            <td>
              <a class="btn" href="{{ $data->id }}" title="Sửa">
                  <i class="fa fa-edit" ></i>
              </a>
              <a class="btn" href="destroy/{{ $data->id }}" title="Xóa">
                  <i class="fa fa-times-circle" ></i>
              </a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="container">
        <div class="row" style="text-align: center;">
               <ul class="pagination">
                    <li><a href="{{url('admin/image?page='.($currentPage-1).'&keyword='.$keyword.'')}}">&laquo;</a></li>
                    @for($i=0;$i<$lastPage;$i++)
                    <li class="{{($i==($currentPage-1))?"active":""}}" ><a href="{{url('admin/homeBlog?page='.($i+1).'&keyword='.$keyword.'')}}">{{$i+1}}</a></li>
                    @endfor
                   <li><a href="{{url('admin/image?page='.($currentPage+1).'&keyword='.$keyword.'')}}">&raquo;</a></li>
                </ul>
              
        </div>
        
@stop


