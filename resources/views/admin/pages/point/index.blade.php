@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/point.css')}}" rel="stylesheet" type="text/css"/>
   
@stop

@section('content')

<div class="container-employee">
<section id="point">
<div id="content-setting-exchange">
    <div id="setting-exchange-content">
        <div id="setting-exchange-body">
            <div><h4><b>THIẾT LẬP QUY ĐỔI ĐIỂM THƯỞNG</b></h4></div>
            <div>Khách hàng sé nhận được điểm thưởng mỗi lần làm dịch vụ hoặc mua hàng tại tiệm,</div>
            <div>khách hàng có thể dùng điểm này để đổi lấy các ưu đãi của tiệm</div>
            <div class="setting-exchange-item" id="setting-exchange-collect-pt">
                <div class="setting-exchange-item-hd" id="setting-exchange-collect-pt-hd">
                    <h4>TÍCH LŨY ĐIỂM THƯỞNG CHO KHÁCH HÀNG</h4>
                </div>
                <div id="setting-exchange-collect-pt-ct">
                                Khách hàng nhận được <b>@{{ point.point }}</b> điểm cho mỗi <b>@{{ point.money }}</b> VND khi làm dịch vụ</div>    
                                <div><a class="btn" data-toggle="modal" data-target="#add-category"  title="Sửa" >
                                    <i class="fa fa-edit" ></i>
                                </a></div> 
            </div>
            <div class="setting-exchange-item" id="setting-exchange-rew-pt">
                <div id="setting-exchange-rew-pt-hd">
                    <h4>PHẦN THƯỞNG CHO KHÁCH HÀNG TÍCH LŨY ĐIỂM THƯỞNG</h4>
                </div>
            </div>
            <div class="setting-exchange-item" id="setting-exchange-rew-pt">
                <div id="setting-exchange-rew-pt-hd">
                    <h4>SỬ DỤNG ĐIỂM THƯỞNG</h4>
                </div>
                <div>Quy đổi điểm thưởng thành cấp bậc</div>
            </div>
             </div>
                </div>   
    
   
    
    
    <div class="modal fade" id="add-category" tabindex="-1" role="dialog">
            			<div class="modal-dialog">
            				<div class="modal-content">
            					<div class="modal-header">
            						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            							<span aria-hidden="true">×</span>
            						</button>
            						<h4 class="modal-title">Thêm</h4>
            					</div>
                                <div class="modal-body">
            						<div class="row">
            							<div class="col-sm-12">
            								<div class="form-horizontal">
            									<div class="form-group" >
            										<label for="name-category" class="col-sm-4 control-label">Số Điểm</label>
            										<div class="col-sm-8">
            											<input type="number" class="form-control" name="point" id="point" placeholder="" v-model="add_point">
            										</div>
            					                </div>
                                                <div class="form-group">
            										<label for="name-category" class="col-sm-4 control-label">Số Tiền</label>
            										<div class="col-sm-8">
            											<input type="number" class="form-control" name="money" id="money" placeholder="" v-model="add_money">
            										</div>
            					                </div>
            					           	</div>
            							</div>
            						</div>
            					</div>
            					<div class="modal-footer">
            						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
            						<button type="button" class="btn btn-primary" @click="createPoint(point.id)" >Cập nhật</button>
            					</div>
            				</div>
                    </div>
                </div>
                 </div>
                
    </div>
    
    
                </section>
        
@stop
@section('js')
    <script src="{{asset('/public/dist/js/point.js')}}"></script>
@stop


