@extends('admin.main')

@section('css')
@endsection

@section('content')
	<section id="service" class="content">
		<div class="row">
			<div class="col-md-5">
				<div class="box">
					<div class="box-header with-border">
						<div class="col-xs-5">
							<h2>Danh mục</h2>
						</div>
						<div class="col-xs-7">
							<button type="button" 
								class="btn bg-maroon btn-flat margin pull-right" 
								data-toggle="modal"
								data-target="#add-category"
								@click="checkService(1)"
							>
								Thêm danh mục
							</button>
						</div>
					</div>

					<div class="box-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th class="col-sm-2">STT</th>
									<th class="col-sm-7">Tên </th>
									<th class="col-sm-3">Chức năng</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="category in categories">
									<td class="col-sm-2">@{{ $index + 1 }}</td>
									<td class="col-sm-7">@{{ category.name }}</td>
									<td class="col-sm-3">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cogs"></i>
											</button>
											<ul class="dropdown-menu pull-right" role="menu">
												<li><a data-toggle="modal" data-target="#add-category" @click="getdataCategory(category.id)" >Chỉnh sửa</a></li>
												<li><a href="javascript:void(0)" @click="deleteCategory(category.id)">Xóa</a></li>
											</ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="box-footer clearfix no-border">
						<div class="box-tools pull-right">
							<ul class="pagination pagination-sm inline">
								<li @click="getCategory(1)"><a>&laquo;</a></li>
								<li v-for="page in category.total_page"  
									@click="getCategory(page + 1)"
									:class="((page + 1) == category.current_page) ? 'active' : ''" 
									><a>@{{ page + 1 }}</a></li>
								<li @click="getCategory(category.last_page)" ><a>&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-7">
				<div class="box">
					<div class="box-header with-border">
						<div class="col-xs-5">
							<div class="form-group">
								<h2>Dịch vụ</h2>
								<select class="form-control"
										v-model="category_id_selected"
										@change="getSubByCategory()"
										>
									<option value="0" >Tất cả danh mục</option>
									<option v-for="category in categories" value="@{{ category.id }}">@{{ category.name }}</option>
								</select>
							</div>
						</div>
						<div class="col-xs-7">
							<button type="button" 
								class="btn bg-maroon btn-flat margin pull-right"
								data-toggle="modal" 
								data-target="#add-category"
								@click="checkService(2)"								
								>Thêm dịch vụ</button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-striped">
						<thead>
							<tr>
								<th class="col-sm-1">STT</th>
								<th class="col-sm-4">Tên </th>
								<th class="col-sm-2">Hoa hồng</th>
								<th class="col-sm-3">Gía</th>
								<th class="col-sm-2">Chức năng</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="sub in sub_categories">
								<td class="col-sm-1">@{{ $index + 1 }}</td>
								<td class="col-sm-4">@{{ sub.name }}</td>
								<td class="col-sm-2">@{{ sub.income }}</td>
								<td class="col-sm-3">@{{ sub.price | number }}</td>
								<td class="col-sm-2">
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-cogs"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a
												data-toggle="modal" 
												data-target="#add-category"
												@click="getSingleSub(sub.id)"
												>Chỉnh sửa</a></li>
											<li><a href="javascript:void(0)" @click="deleteSubCategory(sub.id)">Xóa</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
						</table>
					</div>
					<div class="box-footer clearfix no-border">
						<div class="box-tools pull-right">
							<ul class="pagination pagination-sm inline">
								<li @click="switchGetPagaSub(1)"><a>&laquo;</a></li>
								<li v-for="page in sub.total_page"  
									@click="switchGetPagaSub(page + 1)"
									:class="((page + 1) == sub.current_page) ? 'active' : ''" 
									><a>@{{ page + 1 }}</a></li>
								<li @click="switchGetPagaSub(sub.last_page)" ><a>&raquo;</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="add-category" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Thêm</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="name-category" class="col-sm-4 control-label">Tên</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="name-category" placeholder="" v-model="add_name_service">
										</div>
					                </div>
					                <div class="form-group" v-if="is_service">
					                	<label for="" class="col-sm-4 control-label">Danh mục @{{ add_selected_service }}</label>
					                	<div class="col-sm-8">
					                		<select class="form-control" v-model="add_selected_service">
												<option v-for="category in categories" 
													value="@{{ category.id }}"
													:selected="(add_selected_service = category.id)"
													>@{{ category.name }}</option>
											</select>
					                	</div>
					                </div>
					                <div v-if="is_service" class="form-group">
										<label for="price" class="col-sm-4 control-label">Gía</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="price" placeholder="" v-model="add_price_service">
										</div>
					                </div>
					                <div v-if="is_service" class="form-group">
										<label for="discount" class="col-sm-4 control-label">Hoa hồng</label>
										<div class="col-sm-8">
											<input type="number" class="form-control" id="discount" placeholder="" v-model="add_income_service">
										</div>
					                </div>
					                <div class="form-group">
										<label for="discount" class="col-sm-4 control-label">Thông tin dịch vụ</label>
										<div class="col-sm-8">
											<textarea class="form-control" rows="3" placeholder="Nhập ..." v-model="add_note_service"></textarea>
										</div>
					                </div>
					           	</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
						<button v-if="!is_edit" type="button" class="btn btn-primary" @click="createService(is_service)">Thêm</button>
						<button v-if="is_edit" type="button" class="btn btn-primary" @click="editService(is_service)">Lưu</button>
					</div>
				</div>
			</div>
		</div>
	</section>
	

@endsection

@section('js')
	<script src="{{asset('/public/dist/js/service.js')}}"></script>
@endsection
