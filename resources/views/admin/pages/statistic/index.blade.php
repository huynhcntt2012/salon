@extends('admin.main')
@section('css')
<link href="{{asset('/public/asset/css/revenue.css')}}" rel="stylesheet" type="text/css"/>
<style>label{
margin-left: 20px;
}
#datepickerfrom{
width:180px; 
margin: 0 20px 20px 20px;
}
#datepickerfrom > span:hover{
cursor: pointer;
}

#datepickerto{
width:180px; 
margin: 0 20px 20px 20px;
}
#datepickerto > span:hover{
cursor: pointer;
}
</style>
<link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
@stop
@section('js')
 <script src="{{asset('/public/dist/js/revenue.js')}}"></script>
 
 <script type="text/javascript">
$(function () {  
    jQuery(function ($)
      {
        $.datepicker.regional["vi-VN"] =
        {
          closeText: "Đóng",
          prevText: "Trước",
          nextText: "Sau",
          currentText: "Hôm nay",
          monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
          monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
          dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
          dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
          dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          weekHeader: "Tuần",
          dateFormat: "dd/mm/yy",
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ""
        };

        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
      });
$("#datepickerfrom").datepicker({         
autoclose: true,         
todayHighlight: true 
}).datepicker('update', new Date());

$("#datepickerto").datepicker({         
autoclose: true,         
todayHighlight: true 
}).datepicker('update', new Date());

});


</script>
@stop
@section('content')
<section id="revenue" class="content">
    <div class="row">
        <div style="background: #804040;">
            <div class="col-lg-7" style="float:right;">
                <div>
                    <button type="button" class="col-lg-2 btn btn-default" style="float:left;" @click="setDateNow()">Hôm nay</button>
                </div>
                <div id="datepickerfrom" style="float:left;" name="datepicker" class="col-lg-2 input-group date" data-date-format="dd-mm-yyyy"> 
                    <input class="form-control" readonly="" v-model="dateFrom" type="text">
                    <span class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i></span> 
                </div>
                <div style="float:left;" id="datepickerto" class="col-lg-2 input-group date" data-date-format="dd-mm-yyyy"> 
                    <input class="form-control" readonly=""  v-model="dateTo" type="text">
                    <span class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i></span> 
                </div>
                <div>
                    <button type="button" class="col-lg-1 btn btn-default" style="float:left;">Lọc</button>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-12" style="background: #FAFAFA;">
        <div class="text-center"><h3><b>THỐNG KÊ DOANH THU THEO DỊCH VỤ</b></h3></div>
        <div id="revenue-text" class="text-right">
            <div id="lable">Tổng Thu:<div id="value">0VND</div></div>
            <div class="clearfix"></div>
            <div id="lable">Hoa Hồng Nhân Viên:<div id="value">0VND</div></div>
            <div class="clearfix"></div>
            <div id="lable">Thực Thu:<div id="value">0VND</div></div>
            <div class="clearfix"></div>
            <div id="printbill" class="col-lg-2">
                <button type="button" class="col-lg-12 btn btn-default"  data-toggle="modal" data-target="#add-category" >In Hoa Don</button>
            </div>
        </div>
         <div class="modal fade" id="add-category" tabindex="-1" role="dialog">
            			<div class="modal-dialog">
            				<div class="modal-content">
            					<div class="modal-header">
            						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            							<span aria-hidden="true">×</span>
            						</button>
            						<h4 class="modal-title">Thông Tin Hóa Đơn</h4>
            					</div>
            
            					<div class="modal-body">
            						<div class="row">
            							<div class="col-sm-12">
            								<div class="form-horizontal">
                                                <div id="revenue-bill" class="col-lg-12">
                                                    <div id="revenue-bill-title">
                                                        <div>Hệ Thống Salon Lê Hiếu</div>
                                                        <div>339 Lê Văn Sỹ, Phường 13, Quận 3, TPHCM</div>
                                                        <div>086-294-3833</div>
                                                    </div>
                                                </div>
                                                <div id="title-bill" class="col-lg-12">
                                                    <div id="lable" class="col-lg-6">
                                                        <div>Mã HĐ</div>
                                                        <div>Ngày</div>
                                                        <div>Khách Hàng</div>
                                                        <div>Điện Thoại</div>
                                                    </div>
                                                    <div id="value" class="col-lg-6">
                                                        <div>Mã HĐ</div>
                                                        <div>Ngày</div>
                                                        <div>Khách Hàng</div>
                                                        <div>Điện Thoại</div>
                                                    </div>
                                                </div>
                                                <div>Thông tin chi tiết</div>
                                                <hr />
                                                <table class="table table-striped">
                        							<thead>
                        								<tr>
                        									<th class="col-sm-2">STT</th>
                        									<th class="col-sm-7">Dịch Vụ</th>
                                                            <th class="col-sm-7">Doanh Thu</th>
                        								</tr>
                        							</thead>
                        							<tbody>
                        								<tr>
                        									<td class="col-sm-2">STT</td>
                        									<td class="col-sm-7">Dịch Vụ</td>
                                                            <td class="col-sm-7">Doanh Thu</td>
                        								</tr>
                                                        <tr>
                        									<td class="col-sm-2">STT</td>
                        									<td class="col-sm-7">Dịch Vụ</td>
                                                            <td class="col-sm-7">Doanh Thu</td>
                        								</tr>
                                                        <tr>
                        									<td class="col-sm-2">STT</td>
                        									<td class="col-sm-7">Dịch Vụ</td>
                                                            <td class="col-sm-7">Doanh Thu</td>
                        								</tr>
                                                        <tr>
                        									<td class="col-sm-2">STT</td>
                        									<td class="col-sm-7">Dịch Vụ</td>
                                                            <td class="col-sm-7">Doanh Thu</td>
                        								</tr>
                        							</tbody>
						                          </table>
                                                <hr />
                                                <div id="title-bill" class="col-lg-12">
                                                    <div id="lable" class="col-lg-6">
                                                        <div>Tổng Tiền</div>
                                                        <div>Giảm Giá</div>
                                                        <div>Thành Tiền</div>
                                                    </div>
                                                    <div id="value" class="col-lg-6">
                                                        <div>Tổng Tiền</div>
                                                        <div>Giảm Giá</div>
                                                        <div>Thành Tiền</div>
                                                    </div>
                                                </div>
            									
                                                
            					           	</div>
            							</div>
            						</div>
            					</div>
            					<div class="modal-footer">
                                    <div id="revenue-footer" class="col-lg-12">
                						<button type="button" class="col-lg-5 btn btn-default pull-left" data-dismiss="modal">Thoát</button>
                						<button type="button" class="col-lg-5 btn btn-default pull-right" >In Hóa Đơn</button>
                                    </div>
            					</div>
            				</div>
                    </div>
                </div>
    </div>
      <div class="row">
        <div id="add-category1" class="col-md-12">
        <div class="box">
            <div class="box-header">
            <table class="table table-striped">
							<thead>
								<tr>
									<th class="col-sm-2">STT</th>
									<th class="col-sm-7">Dịch Vụ</th>
                                    <th class="col-sm-7">Doanh Thu</th>
								</tr>
							</thead>
							<tbody>
								<!--<tr v-for="cus in customerlook">
									<td class="col-sm-2">@{{ $index + 1 }}</td>
									<td class="col-sm-7">@{{ cus.name }}</td>
                                    <td class="col-sm-7">@{{ cus.phone }}</td>
									<td class="col-sm-3">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cogs"></i>
											</button>
											<ul class="dropdown-menu pull-right" role="menu">
												<li><a data-toggle="modal" data-target="#add-category" @click="getdataCategory(category.id)" >Chỉnh sửa</a></li>
												<li><a href="javascript:void(0)" @click="deleteCategory(category.id)">Xóa</a></li>
											</ul>
										</div>
									</td>
								</tr>-->
							</tbody>
						</table>
        
                  </div>
                </div>
            </div>
        <!-- /.col-->
      </div>
          <!-- ./row -->
</section>
        
@stop




