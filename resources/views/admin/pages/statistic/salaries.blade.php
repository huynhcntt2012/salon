@extends('admin.main')
@section('css')
<link href="{{asset('/public/asset/css/revenue.css')}}" rel="stylesheet" type="text/css"/>
<style>label{
margin-left: 20px;
}
#datepickerfrom{
width:180px; 
margin: 0 20px 20px 20px;
}
#datepickerfrom > span:hover{
cursor: pointer;
}

#datepickerto{
width:180px; 
margin: 0 20px 20px 20px;
}
#datepickerto > span:hover{
cursor: pointer;
}
</style>
<link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
@stop
@section('js')
 
 
 <script type="text/javascript">
$(function () {  
    jQuery(function ($)
      {
        $.datepicker.regional["vi-VN"] =
        {
          closeText: "Đóng",
          prevText: "Trước",
          nextText: "Sau",
          currentText: "Hôm nay",
          monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
          monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
          dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
          dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
          dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          weekHeader: "Tuần",
          dateFormat: "dd/mm/yy",
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ""
        };

        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
      });
$("#datepickerfrom").datepicker({         
autoclose: true,         
todayHighlight: true 
}).datepicker('update', new Date());

$("#datepickerto").datepicker({         
autoclose: true,         
todayHighlight: true,
format: "mm-yyyy",
viewMode: "months", 
minViewMode: "months"
}).datepicker('update', new Date());
$("#datepickerto").datepicker.setDefaults($.datepicker.regional["vi-VN"]);
});


</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@stop
@section('content')
<section>
<div class="row">
    <div style="background: #804040;">
        <div class="col-lg-4" style="float:right;">
            
            <div style="float:left;" id="datepickerto" class="col-lg-4 input-group date" data-date-format="dd-mm-yyyy"> 
                <input class="form-control" readonly="" type="text">
                <span class="input-group-addon">
                <i class="glyphicon glyphicon-calendar"></i></span> 
            </div>
            <div>
                <button type="button" class="col-lg-2 btn btn-default" style="float:left;">Lọc</button>
            </div>
            
        </div>
    </div>
</div>
<div class="col-lg-12" style="background: #FAFAFA;">
    <div class="text-center"><h3><b>THỐNG KÊ LƯƠNG NHÂN VIÊN</b></h3></div>
    <div id="revenue-text" class="text-right">
        <div id="lable">Tổng Thu:<div id="value">0VND</div></div>
        <div class="clearfix"></div>
        <div id="lable">Hoa Hồng Nhân Viên:<div id="value">0VND</div></div>
        <div class="clearfix"></div>
        <div id="lable">Thực Thu:<div id="value">0VND</div></div>
    </div>
</div>
<section>
      <div class="row">
        <div id="add-category" role="dialog" class="col-md-12">
        <div class="box">
            <div class="box-header">
            <table class="table table-striped">
							<thead>
								<tr>
									<th class="col-sm-2">STT</th>
									<th class="col-sm-7">Dịch Vụ</th>
                                    <th class="col-sm-7">Doanh Thu</th>
								</tr>
							</thead>
							<tbody>
								<!--<tr v-for="cus in customerlook">
									<td class="col-sm-2">@{{ $index + 1 }}</td>
									<td class="col-sm-7">@{{ cus.name }}</td>
                                    <td class="col-sm-7">@{{ cus.phone }}</td>
									<td class="col-sm-3">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cogs"></i>
											</button>
											<ul class="dropdown-menu pull-right" role="menu">
												<li><a data-toggle="modal" data-target="#add-category" @click="getdataCategory(category.id)" >Chỉnh sửa</a></li>
												<li><a href="javascript:void(0)" @click="deleteCategory(category.id)">Xóa</a></li>
											</ul>
										</div>
									</td>
								</tr>-->
							</tbody>
						</table>
          </div>
        </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section> 

</section>
        
@stop




