@extends('admin.main')
@section('css')
	<link  rel="stylesheet" href="{{asset('/public/dist/css/select2/select2.min.css')}}">
	<style type="text/css">
		.select2-selection__rendered {
			line-height: inherit !important;
		}
		.nav > li > a:hover, .nav > li > a:active, .nav > li > a:focus {
			/* color: inherit !important; */
			background: inherit !important;
		}
		.bg-green a {
			color: #FFF;
		}
	</style>
@endsection
@section('content')
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
		        <div class="box">
		            <div class="box-header">
		              	<h3 class="box-title">Tạo phiếu thu chi</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<table id="examplé" class="table table-bordered table-striped">
			                <thead>
				                <tr>
					                <th class="col-md-1">STT</th>
					                <th class="col-md-3">Mã</th>
					                <th class="col-md-3">Tên</th>
					                <th class="col-md-3">Số lượng</th>
					                <th class="col-md-2"></th>
				                </tr>
			                </thead>
			                <tbody>
			                <tr>
			                	<td style="vertical-align: middle">Stt 1</td>
			                  	<td>
			                  		<div class="input-group">
				                      	<input type="text" placeholder="Mã..." class="form-control">
				                        <span class="input-group-btn">
				                           <button type="button" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></button>
				                           <button type="button" class="btn btn-success btn-flat" data-toggle="tooltip" title="" data-original-title="Chọn"><i class="fa fa-check"></i></button>
				                        </span>
				                    </div>
			                  	</td>
			                  	<td style="vertical-align: middle">
			                  		Name
			                  		
			                  	</td>
			                  	<td>
			                  		<input type="number" placeholder="0" min="0" class="form-control">
			                  	</td>
			                 	<td>
			                 		<button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="" data-original-title="Xong"><i class="fa fa-check"></i></button>
			                 		<button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></button>
			                 		<button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="" data-original-title="Xóa"><i class="fa fa-times"></i></button>
			                 	</td>
			                </tr>
			                <tr>
			                	<td style="vertical-align: middle">Stt 1</td>
			                  	<td style="vertical-align: middle">
			                  		Bà cô khó ưa
			                  	</td>
			                  	<td style="vertical-align: middle">
			                  		Name
			                  	</td>
			                  	<td>
			                  		10
			                  	</td>
			                 	<td>
			                 		<button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="" data-original-title="Xong"><i class="fa fa-check"></i></button>
			                 		<button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></button>
			                 		<button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="" data-original-title="Xóa"><i class="fa fa-times"></i></button>
			                 	</td>
			                </tr>
			                </tfoot>
		                </table>
		                <button type="button" class="btn btn-block btn-info btn-flat" style="opacity: 0.4">
		                	<i class="fa fa-plus"></i>
		                </button>

		                <button type="button" class="btn btn-success margin pull-right">Tạo</button>

		            	<button type="button" class="btn btn-danger margin pull-right">Hủy</button>
		            </div>
		            <!-- /.box-body -->
		            
		        </div>
          		<!-- /.box -->
        	</div>
		</div>
{{-- tabindex="-1" --}}
		<div class="modal fade no-print" id="myModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Tìm tên sản phẩm</h4>
					</div>

					<div class="modal-body">
						<div class="row">
            				<div class="col-md-6">
              					<div class="form-group">
                					<label>Nhà sản xuất:</label>
            						<select class="form-control select2" style="width: 100%;">
              							<option selected="selected">Alabama</option>
						                <option>Alaska</option>
						                <option>California</option>
						                <option>Delaware</option>
						                <option>Tennessee</option>
						                <option>Texas</option>
          								<option>Washington</option>
            						</select>
              					</div>
              				</div>
              				<div class="col-md-6">
              					<div class="form-group">
                					<label>Loại hàng:</label>
            						<select class="form-control select2" style="width: 100%;">
              							<option selected="selected">Alabama</option>
						                <option>Alaska</option>
						                <option>California</option>
						                <option>Delaware</option>
						                <option>Tennessee</option>
						                <option>Texas</option>
          								<option>Washington</option>
            						</select>
              					</div>
              				</div>
              			</div>
              			<div class="row">
              				<div class="col-md-12">
              					<label>Tên sản phẩm:</label>
              					<input type="text" class="form-control" placeholder="Tên sản phẩm">
              				</div>
              			</div>
              			<div class="row" style="margin-top: 10px">
              				<div class="col-md-12">
              					<label>Kết quả:</label>
              					<div class="box box-solid box-primary result-name">
              						<ul class="nav nav-stacked">
						            	<li class="bg-green"><a href="#">Không có kết quả <span class="pull-right badge bg-blue">31</span></a></li>
						            	<li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
						            	<li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
						            	<li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
						            	<li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
						            	<li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
						            	<li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
						            </ul>
              					</div>	
              				</div>
              			</div>
            		</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Xong</button>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('/public/dist/js/select2/select2.min.js')}}"></script>
{{-- <script src="{{asset('/public/dist/js/bill.js')}}"></script> --}}
<script>
	$('#datepicker-start-day').datepicker({
      autoclose: true
    });
    $('#datepicker-end-day').datepicker({
      autoclose: true
    });
    $(".select2").select2();
    $('.result-name').slimScroll({
		height: '165px'
	});
</script>
@endsection
