@extends('admin.main')
@section('css')
{{-- <link  rel="stylesheet" href="{{asset('/public/dist/css/.css')}}"> --}}
@endsection
@section('content')
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<div class="col-md-4">
							<label>Ngày bắt đầu:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right" id="datepicker-start-day">
							</div>
						</div>
						
						<div class="col-md-4">
							<label>Ngày kết thúc:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right" id="datepicker-end-day" >
							</div>
						</div>

						<div class="col-md-4" style="margin-top: 14px;">
								<a href="{{ route('stock.goods.create') }}" type="button" class="btn bg-success btn-flat margin pull-right">Tạo phiếu</a>
								<button type="button" class="btn bg-default btn-flat margin pull-right">Xóa lọc</button>
								<button type="button" class="btn bg-maroon btn-flat margin pull-right">Lọc</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box" >
					<div class="box-header">
						<h3 class="box-title">Danh sách phiếu nhập kho</h3>
						
						<div class="box-tools">
							<ul class="pagination pagination-sm no-margin pull-right">
								<li><a href="#">&laquo;</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding" style="height: 500px;">
						<table class="table" >
							<tr>
								<th style="width: 10px">STT</th>
								<th style="width: 120px">Mã</th>
								<th style="width: 160px">Ngày tạo</th>
								<th style="width: 70px">Người tạo</th>
								<th style="width: 70px">Chức năng</th>
							</tr>
							
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									{{-- <div class="progress progress-xs">
										<div class="progress-bar progress-bar-danger" style="width: 55%"></div>
									</div> --}}
									19:00 09-07-2017
								</td>
								<td>
									<a href="javascript:void(0)">@Admin</a>
								</td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>2.</td>
								<td>Clean database</td>
								<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
								</div>
								</td>
								<td><span class="badge bg-yellow">70%</span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>3.</td>
								<td>Cron job running</td>
								<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
								</div>
								</td>
								<td><span class="badge bg-light-blue">30%</span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>4.</td>
								<td>Fix and squish bugs</td>
								<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-success" style="width: 90%"></div>
								</div>
								</td>
								<td><span class="badge bg-green">90%</span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>

			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Danh sách phiếu xuất kho</h3>
						
						<div class="box-tools">
							<ul class="pagination pagination-sm no-margin pull-right">
								<li><a href="#">&laquo;</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding" style="height: 500px;">
						<table class="table">
							<tr>
								<th style="width: 10px">STT</th>
								<th style="width: 120px">Mã</th>
								<th style="width: 160px">Ngày tạo</th>
								<th style="width: 70px">Người tạo</th>
								<th style="width: 70px">Chức năng</th>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>1.</td>
								<td>Update software</td>
								<td>
									19:00 09-07-2017
								</td>
								<td><a href="javascript:void(0)">@Admin</a></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>2.</td>
								<td>Clean database</td>
								<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
								</div>
								</td>
								<td><span class="badge bg-yellow">70%</span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>3.</td>
								<td>Cron job running</td>
								<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
								</div>
								</td>
								<td><span class="badge bg-light-blue">30%</span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-wrench"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#">Xem chi tiết</a></li>
											<li><a href="#">Sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
                				</td>
							</tr>
							<tr>
								<td>4.</td>
								<td>Fix and squish bugs</td>
								<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-success" style="width: 90%"></div>
								</div>
								</td>
								<td><span class="badge bg-green">90%</span></td>
							</tr>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
{{-- <script src="{{asset('/public/dist/js/bill.js')}}"></script> --}}
<script>
	$('#datepicker-start-day').datepicker({
      autoclose: true
    });
    $('#datepicker-end-day').datepicker({
      autoclose: true
    });
	$('#height-stock-goods').slimScroll({
		height: '443px'
	});
	$('#height-stock-goods1').slimScroll({
		height: '443px'
	});

</script>
@endsection
