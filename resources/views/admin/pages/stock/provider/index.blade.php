@extends('admin.main')
@section('css')
{{-- <link  rel="stylesheet" href="{{asset('/public/dist/css/.css')}}"> --}}
@endsection
@section('content')
	<section class="content">
		<div class="row">
{{-- 			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<div class="col-md-4">
							<label>Ngày bắt đầu:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right" id="datepicker-start-day">
							</div>
						</div>
						
						<div class="col-md-4">
							<label>Ngày kết thúc:</label>
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right" id="datepicker-end-day" >
							</div>
						</div>

						<div class="col-md-4" style="margin-top: 14px;">
								<a href="{{ route('stock.voucher.create') }}" type="button" class="btn bg-success btn-flat margin pull-right">Tạo phiếu</a>
								<button type="button" class="btn bg-default btn-flat margin pull-right">Xóa lọc</button>
								<button type="button" class="btn bg-maroon btn-flat margin pull-right">Lọc</button>
						</div>
					</div>
				</div>
			</div> --}}
			
			<div class="col-xs-12">
		        <div class="box">
		            <div class="box-header">
		              	<h3 class="box-title">Danh sách nhà cung cấp</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<table id="examplé" class="table table-bordered table-striped">
			                <thead>
				                <tr>
					                <th class="col-md-1">STT</th>
					                <th class="col-md-3">Mã</th>
					                <th class="col-md-3">Tên</th>
					                <th class="col-md-3">Ngày nhập</th>
					                <th class="col-md-2"></th>
				                </tr>
			                </thead>
			                <tbody>
			                <tr>
			                	<td style="vertical-align: middle">Stt 1</td>
			                  	<td style="vertical-align: middle">
			                  		CAP97
			                  	</td>
			                  	<td style="vertical-align: middle">
			                  		Name
			                  	</td>
			                  	<td>
			                  		19:00 08-07-2017
			                  	</td>
			                 	<td>
			                 		<a href="{{ route('stock.provider.detail', 12) }}" type="button" class="btn btn-success btn-sm" data-toggle="tooltip"  title="Xem chi tiết" data-original-title="Xem chi tiết"><i class="fa fa-eye"></i></a>
			                 		<button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></button>
			                 		<button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="" data-original-title="Xóa"><i class="fa fa-times"></i></button>
			                 	</td>
			                </tr>
			                <tr>
			                	<td style="vertical-align: middle">Stt 1</td>
			                  	<td style="vertical-align: middle">
			                  		Bà cô khó ưa
			                  	</td>
			                  	<td style="vertical-align: middle">
			                  		Name
			                  	</td>
			                  	<td>
			                  		10:00 29-07-2017
			                  	</td>
			                 	<td>
				                 	<button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" title="" data-original-title="Xem chi tiết"><i class="fa fa-eye"></i></button>
			                 		<button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></button>
			                 		<button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="" data-original-title="Xóa"><i class="fa fa-times"></i></button>
			                 	</td>
			                </tr>
			                </tfoot>
		                </table>
		            </div>
		            <!-- /.box-body -->

		            <div class="box-footer">
		            	<div class="box-tools">
							<ul class="pagination pagination-sm no-margin pull-right">
								<li><a href="#">&laquo;</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</div>		            
		            </div>
		        </div>
          		<!-- /.box -->
        	</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
{{-- <script src="{{asset('/public/dist/js/bill.js')}}"></script> --}}
<script>
	$('#datepicker-start-day').datepicker({
      autoclose: true
    });
    $('#datepicker-end-day').datepicker({
      autoclose: true
    });
	$('#height-stock-goods').slimScroll({
		height: '443px'
	});
	$('#height-stock-goods1').slimScroll({
		height: '443px'
	});

</script>
@endsection
