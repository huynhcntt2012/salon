@extends('admin.main')
@section('css')
{{-- <link  rel="stylesheet" href="{{asset('/public/dist/css/.css')}}"> --}}
@endsection
@section('content')
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
		        <div class="box">
		            <div class="box-header">
		              	<h3 class="box-title">Chi tiết nhà cung cấp</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tên nhà sản xuất</label>
								<input type="text" class="form-control" placeholder="Enter ..." disabled="disabled">
							</div>
							<div class="form-group">
								<label>Địa chỉ nhà sản xuất</label>
								<input type="text" class="form-control" placeholder="Enter ..." disabled="disabled">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tên nhà sản xuất</label>
								<input type="text" class="form-control" placeholder="Enter ..." disabled="disabled">
							</div>
						</div>
		            </div>
		            <!-- /.box-body -->
		        </div>
          		<!-- /.box -->
        	</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
{{-- <script src="{{asset('/public/dist/js/bill.js')}}"></script> --}}
<script>
	$('#datepicker-start-day').datepicker({
      autoclose: true
    });
    $('#datepicker-end-day').datepicker({
      autoclose: true
    });
	$('#height-stock-goods').slimScroll({
		height: '443px'
	});
	$('#height-stock-goods1').slimScroll({
		height: '443px'
	});

</script>
@endsection
