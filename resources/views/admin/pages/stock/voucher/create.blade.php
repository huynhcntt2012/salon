@extends('admin.main')
@section('css')
	<link  rel="stylesheet" href="{{asset('/public/dist/css/select2/select2.min.css')}}">
	<style type="text/css">
		.select2-selection__rendered {
			line-height: inherit !important;
		}
		.nav > li > a:hover, .nav > li > a:active, .nav > li > a:focus {
			/* color: inherit !important; */
			background: inherit !important;
		}
		.bg-green a {
			color: #FFF;
		}
		table tbody tr td {
			vertical-align: middle !important;
		}
	</style>
@endsection
@section('content')
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
		        <div class="box">
		            <div class="box-header">
		              	<h3 class="box-title">Tạo phiếu thu chi</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<table id="examplé" class="table table-bordered table-striped">
			                <thead>
				                <tr>
					                <th class="col-md-2">Tên sàn phẩm</th>
					                <th class="col-md-2">Tên nhân viên </th>
					                <th class="col-md-1">Số lượng</th>
					                <th class="col-md-2">Đơn gía (VND)</th>
									<th class="col-md-2">Tổng tiền (VND)</th>
									<th class="col-md-3">Ghi chú</th>
				                </tr>
			                </thead>
			                <tbody>
			                <tr>
			                  	<td>
			                  		<input type="text" name="" class="form-control">
			                  	</td>
			                  	<td style="vertical-align: middle">
			                  		@Nguyễn Ngọc Bích
			                  	</td>
			                  	<td>
			                  		<input type="number" placeholder="0" min="0" class="form-control">
			                  	</td>
			                 	<td>
				                 	<input type="number" placeholder="0" min="0" class="form-control">
			                 	</td>
			                 	<td>
			                 		<input type="number" placeholder="0" min="0" class="form-control">
			                 	</td>
			                 	<td>
			                 		<textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
			                 	</td>
			                </tr>
			                </tfoot>
		                </table>

		                <button type="button" class="btn btn-success margin pull-right">Tạo</button>

		            	<button type="button" class="btn btn-danger margin pull-right">Hủy</button>
		            </div>
		            <!-- /.box-body -->
		            
		        </div>
          		<!-- /.box -->
        	</div>
		</div>
	</section>
@endsection

@section('js')
<script src="{{asset('/public/dist/js/moment.min.js')}}"></script>
<script src="{{asset('/public/dist/js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('/public/dist/js/select2/select2.min.js')}}"></script>
{{-- <script src="{{asset('/public/dist/js/bill.js')}}"></script> --}}
<script>
	$('#datepicker-start-day').datepicker({
      autoclose: true
    });
    $('#datepicker-end-day').datepicker({
      autoclose: true
    });
    $(".select2").select2();
    $('.result-name').slimScroll({
		height: '165px'
	});
</script>
@endsection
