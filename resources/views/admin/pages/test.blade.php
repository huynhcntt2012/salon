@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
        
    </script>
@stop
@section('content')
<h2>B�i vi?t <a class="btn btn-success" href="createblog">T?o tin1</a></h2>
    
<form id="frm_search" action="{{url('admin/homeBlog')}}" method="Get" class="form-inline" style="padding-right:5px;margin-bottom: 10px;">
        <div class="form-group">
          <label for="email">Ti�u d?</label>
          <input type="text" class="form-control" name="keyword" value="{{$keyword}}"  id="keyword">
        </div>
        <div class="form-group">
          <label for="pwd">V? tr�</label>
          <select class="form-control" name="status"  id="status">
              <?php 
                if($status==""){
                    echo '<option selected="selected" value=""></option> <option value="home">Trang ch?</option> <option value="blog">Trang Tin t?c</option>';
                }
                if($status=="home"){
                    echo '<option  value=""></option> <option selected="selected" value="home">Trang ch?</option> <option value="blog">Trang Tin t?c</option>';
                }
                if($status=="blog"){
                    echo '<option  value=""></option> <option value="home">Trang ch?</option> <option selected="selected" value="blog">Trang Tin t?c</option>';
                }
              ?>
             
             
                     
          </select>
        </div>
    <button type="submit" id="submit" class="btn btn-default">T�m ki?m</button>
    </form> 
        
        
    <table class="table table-blog" style="background-color: #fff ">
      <thead>
        <tr>
          <th>
              Stt         
          </th>
          <th>
              Ti�u d?            
          </th>
          <th>Ngu?i t?o</th>
          <th>Ng�y t?o</th>
          <th>V? tr�</th>
        </tr>
      </thead>
      <tbody>
      <?php $stt = 1;?>
          @foreach ($posts as $posts)

            <tr>
                <td>{{$stt++}}</td>
            <td>{{$posts->post_title}}
                <div class="box-control-table">
                 <div class="control-table">
                     <a href="{{asset('admin/createblog/'.$posts->ID.'')}}">C?p Nh?t</a> |
                    <a data-href="{{asset('admin/deleteblog/'.$posts->ID.'')}}" data-toggle="modal" data-target="#confirm-delete" >X�a</a>
                </div>
              </div>
            </td>
            <td>{{$posts->post_name}}</td>
            <td>{{$posts->post_date}}</td>
            <td>{{$posts->post_status}}</td>
          </tr>       
        @endforeach

       
      </tbody>
    </table> 
    <div class="container">
        <div class="row" style="text-align: center;">
               <ul class="pagination">
                    <li><a>&laquo;</a></li>
                    @for($i=0;$i<$lastPage;$i++)
                    <li class="{{($i==($currentPage-1))?"active":""}}" ><a href="{{asset('admin/homeBlog?page='.($i+1).'&keyword='.$keyword.'&status='.$status.'')}}">{{$i+1}}</a></li>
                    @endfor
                   <li><a>&raquo;</a></li>
                </ul>
              
        </div>
      <div class="row">
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Th�ng b�o</h4>
                </div>
            
                <div class="modal-body">              
                    <p>B?n mu?n x�a b�i vi?t n�y ?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kh�ng</button>
                    <a class="btn btn-danger btn-ok">�?ng �</a>
                </div>
            </div>
        </div>
    </div>
        
@stop


