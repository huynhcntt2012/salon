@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
   
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>    
    
@stop
@section('content')

<div class="row">   
        <div class="col-lg-9">
            <h2>Thông Tin Ảnh </h2>
            <form action="{{url('admin/image')}}" enctype="multipart/form-data" method="POST">
                <div class="form-group">
                  <label for="title">Tên Bức Ảnh</label>
                  <input type="text" class="form-control" value='{{$post[0]->post_title or ""}}' name="title" id="title">
                </div>
                <div class="form-group">
                  <label for="title">Chủ Để Ảnh</label>
                  <select id="cate" name="cate" class="form-control">
                   @foreach($category as $cate)
                   <option value="{{$cate->id}}">{{$cate->name}}</option>
                   @endforeach
                  </select>
                </div>
                <input type="hidden" name ="name-img" id="name-img" value='{{$post[0]->post_image or ""}}'  />
                <button type="submit" class="btn btn-primary">save</button>
            </form>
        </div>
        <div  class="col-lg-3">
            <h2>Image represent</h2>
            <div class="box-img-rep">
                <img id="img_represent" name="img_represent" src="{{asset('/public/images/'.(isset($post[0]->post_image)?$post[0]->post_image:'noimage.jpg').'')}}" class="img-responsive" alt="Cinque Terre">
            </div>            
            <br>
            <button class="btn btn-default" data-toggle="modal" data-target="#myModal">Set Image</button>
        </div>
        
    
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Library image <span class="status-upload">Upload Fail</span></h4>    
           
      </div>
        <div class="modal-body" >
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Tải ảnh lên</a></li>
            <li><a data-toggle="tab" href="#menu1" onclick="getImgaes()" >Hình ảnh</a></li>
            
          </ul>

          <div class="tab-content" style="margin-top: 20px;position:relative;">
            <input type="hidden" id="name-img-upload"/>
            <div id="home" class="tab-pane fade in active" >
                
                <div class="row">
                    <div class="col-lg-12">                       
                            <div class="row">
                                <div id="load_screen">
                                    <div id="loading">

                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center">
                                        <input type="file" id="img"/>
                                        <button id="upload" class="btn btn-primary" onclick="upload()" >Upload</button>
                                </div>
                            </div>                        
                    </div>
                </div>
            </div>
              <div id="menu1" class="tab-pane fade" >
                <div class="row" id="box-imgs">
                    <div class="col-lg-3 box-img">
                        <img src="{{asset('/public/images/1488256509.jpg')}}" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">                      
                            <input type="radio" name="img_check" class="checkbox"/>			
                        </div>
                    </div>
                </div>
            </div>          
          </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="setImgS()">Save changes</button>
      </div>
    </div>
  </div>
</div>
@stop