@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('js')
    <script src="{{asset('/public/dist/js/video.js')}}"></script>
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            
            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
        
    </script>
@stop
@section('content')
<section id="video" class="content">
		<div class="row">
            <h2>Kho Video
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-category" @click="checkService(2)">Upload Video</button>
            </h2>
            <form id="frm_search" action="{{url('admin/video')}}" method="Get" class="form-inline" style="padding-right:5px;margin-bottom: 10px;">
                    <div class="form-group">
                      <label for="email">Tiêu đề</label>
                      <input type="text" class="form-control" name="keyword" value="{{$keyword}}"  id="keyword">
                    </div>
                <button type="submit" id="submit" class="btn btn-default">Tìm kiếm</button>
              </form> 
                    
                    
                <table class="table table-blog" style="background-color: #fff ">
                  <thead>
                    <tr>
                      <th>
                          Stt         
                      </th>
                      <th>
                          Tiêu đề            
                      </th>
                      <th>
                          link youtube         
                      </th>
                      <th>Người tạo</th>
                      <th>Ngày tạo</th>
                      <th>Tùy Chọn</th>
                    </tr>
                  </thead>
                    <tbody>
                        <tr v-for="video1 in video">
                            <td class="col-sm-1">@{{ $index + 1 }}</td>
                            <td class="col-sm-2">@{{ video1.title }}</td>
                            <td class="col-sm-4">@{{ video1.link_youtube }}</td>
                            <td class="col-sm-1">@{{ video1.creater }}</td>
                            <td class="col-sm-2">@{{ video1.created_at }}</td>
                            <td class="col-sm-2">
                                <a class="btn" data-toggle="modal" data-target="#add-category" @click="getSingleSub(video1.id)" title="Sửa" >
                                    <i class="fa fa-edit" ></i>
                                </a>
                              
                                <a class="btn" @click="deletevideo(video1.id)" title="Xóa" >
                                    <i class="fa fa-times-circle" ></i>
                                </a>
                            </td>
                        </tr>
                  </tbody>
                </table>
                <div class="container">
                    <div class="row" style="text-align: center;">
                            <ul class="pagination ">
								<li @click="switchPage(1)"><a>&laquo;</a></li>
								<li v-for="page in video1.total_page"  @click="switchPage(page + 1)" :class="((page + 1) == video.current_page) ? 'active' : ''">
                                    <a>@{{ page + 1 }}</a></li>
								<li @click="switchPage(video.last_page)" ><a>&raquo;</a></li>
							</ul>
                          
                    </div>
							
						
                <div class="row">
                  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Thông báo</h4>
                            </div>
                        
                            <div class="modal-body">              
                                <p>Bạn muốn xóa bài viết này ?</p>
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                                <a class="btn btn-danger btn-ok" >Đồng ý</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="add-category" tabindex="-1" role="dialog">
            			<div class="modal-dialog">
            				<div class="modal-content">
            					<div class="modal-header">
            						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            							<span aria-hidden="true">×</span>
            						</button>
            						<h4 class="modal-title">Thêm</h4>
            					</div>
            
            					<div class="modal-body">
            						<div class="row">
            							<div class="col-sm-12">
            								<div class="form-horizontal">
            									<div class="form-group" v-if="isvideo">
            										<label for="name-category" class="col-sm-4 control-label">Tiêu Đề</label>
            										<div class="col-sm-8">
            											<input type="text" class="form-control" id="title" placeholder="" v-model="add_title">
            										</div>
            					                </div>
                                                <div class="form-group">
            										<label for="name-category" class="col-sm-4 control-label">Link Youtube</label>
            										<div class="col-sm-8">
            											<input type="text" class="form-control" id="link_youtube" placeholder="" v-model="add_link_youtube">
            										</div>
            					                </div>
            					           	</div>
            							</div>
            						</div>
            					</div>
            					<div class="modal-footer">
            						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
            						<button v-if="!is_edit" type="button" class="btn btn-primary" @click="createvideo()">Thêm</button>
            						<button v-if="is_edit" type="button" class="btn btn-primary" @click="editvideo()">Lưu</button>
            					</div>
            				</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        
@stop




