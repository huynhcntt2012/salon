<div class="footer-full-width-area-wrap">
    <div class="container">
    <section id="footer-full-width-area" class="footer-full-width-area widget-area"><aside id="durand_widget_subscribe_follow-2" class="widget widget-subscribe"><div class="subscribe-block">
    <h2 class="widget-title">Get Our special offers</h2> <h4 class="subscribe-block__message">and the ultimate hair care tips &amp; tricks!</h4>
    <form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="34f8ef18eb"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
    <div class="subscribe-block__success hidden">You successfully subscribed</div>
    <div class="subscribe-block__error hidden"></div>
    </div></form>
    </div><div class="follow-block"><h2 class="widget-title">Let's Stay Connected</h2><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list">
	<li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
    
    <!--
	<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
	<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
    <li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
    <li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
	-->
	
    </ul></div></div></aside></section> 
    </div>
</div>
<div class="footer-area-wrap invert">
    <div class="container">
        <section id="footer-area" class="footer-area widget-area row"><aside id="durand_widget_about-2" class="col-xs-12 col-sm-6 col-md-3  widget widget-about">
            <div class="widget-about__logo">
                <a class="widget-about__logo-link" href="https://ld-wp.template-help.com/wordpress_58991/">
                <img class="widget-about__logo-img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/logo_about.png" alt="Durand">
                </a>
            </div>
            <div class="widget-about__tagline"></div>
            <!--
			<div class="widget-about__content">
			
			<div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<p>Yearly we help thousands of women in New York to highlight their beauty, care about their precious hair and all in all make them feel special!</p>
			</div>
			</div>
			</div>
			-->
			
            </aside>
            <aside id="nav_menu-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_nav_menu">
                <h5 class="widget-title">navigation</h5>
                <!--
				<div class="menu-custom-menu-container">
				<ul id="menu-custom-menu" class="menu">
				<li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://ld-wp.template-help.com/wordpress_58991/about-us/">About us</a></li>
				<li id="menu-item-1066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1066"><a href="https://ld-wp.template-help.com/wordpress_58991/services/">Services</a></li>
				<li id="menu-item-1064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/">Our staff</a></li>
				<li id="menu-item-1063" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1063"><a href="https://ld-wp.template-help.com/wordpress_58991/schedule/">Schedule</a></li>
				<li id="menu-item-1065" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1065"><a href="https://ld-wp.template-help.com/wordpress_58991/news/">News</a></li>
				<li id="menu-item-1618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1618"><a href="https://ld-wp.template-help.com/wordpress_58991/contacts/">Contacts</a></li>
				</ul>
				</div>
				-->
            </aside>
            <aside id="text-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_text">
                <h5 class="widget-title">Contact Information</h5>
                <!--
					<div class="textwidget">
					<p>4096 N Highland St, Arlington, VA</p>
					<p>32101, USA</p>
					<a href="mailto:ironmass@demolink.org">durand@demolink.org</a>
					</div>			
					-->
            </aside>
            <aside id="text-5" class="col-xs-12 col-sm-6 col-md-3  widget widget_text">
                <h5 class="widget-title">We are open</h5> 
				<!--
				<div class="textwidget">
				<p>Mon-Thu: 9:30 - 21:00</p>
				<p>Fri: 6:00 - 21:00</p>
				<p>Sat: 10:00 - 15:00</p>
				</div>
				-->
            </aside>
        </section> 
    </div>
</div>
<div class="footer-container">
    <div class="site-info container">
        <div class="site-info__mid-box">
        <div class="footer-copyright">© 2017 All rights reserved by DURAND</div>
            <nav id="footer-navigation" class="footer-menu" role="navigation">
            </nav> 
        </div>
    </div> 
</div> 
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
		<span class="glyphicon glyphicon-chevron-up">
		</span>
</a>            		
                    
    <!-- Bootstrap Core JavaScript -->
	<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <script src="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/assets/nivoslider/jquery.nivo.slider.js')}}"></script>
    
	<script>
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
    </script>
	<script type="text/javascript">
	$(document).ready(function(){
			 $(window).scroll(function () {
					if ($(this).scrollTop() > 50) {
						$('#back-to-top').fadeIn();
					} else {
						$('#back-to-top').fadeOut();
					}
				});
				// scroll body to 0px on click
				$('#back-to-top').click(function () {
					$('#back-to-top').tooltip('hide');
					$('body,html').animate({
						scrollTop: 0
					}, 800);
					return false;
				});
				
				//$('#back-to-top').tooltip('show');

		});
		$(window).load(function() {
			$("#load_screen").css("display","none");
		});
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
		var flag = 0;
		var flag1 = 0;
		$(".menu-toggle").click(function(){

			if(flag == 0){
				$(".main-navigation").addClass("toggled");
				flag = 1 ;
			}else{
				$(".main-navigation").removeClass("toggled");
				flag = 0 ;
			}		
		});
		$(".sub-menu-toggle").click(function(){
			if(flag == 0){
				$(this).addClass("active");
				$(this).parent().addClass("sub-menu-open");
				flag = 1 ;
			}else{
				$(this).parent().removeClass("sub-menu-open");
				$(this).removeClass("active");
				flag = 0 ;
			}			
		});
		$(".tm-pb-arrow-next").off().on("click",function(){
			 $(".tm_pb_slide").each(function(){
				var index = $(".tm_pb_slide").index(this);
				if(index == 0){
					$(this).addClass("tm-pb-moved-slide");
				}
				if(index ==1){
					$(this).addClass("tm-pb-active-slide");	
				}
				if(index ==2){
					$(this).css("z-index",1)
				}
				if($(this).is(':visible')){
					var index = $('.tm_pb_slide').index(this);
					console.log(index);
				}
				console.log(index);
			 });
			 
			
		});
	</script>