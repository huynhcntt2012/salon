<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/css/index.css')}}">
    <?php 
        $pathThemes = explode('/',$arrayBase['url']); 
        $path = '';
        if(count($pathThemes) > 1){
            for($i = 0; $i < (count($pathThemes) -1); $i++){
                $path .= '../';
            }
        }
    ?>
    <?php
        switch($arrayBase['url']){
            case 'home':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/home.css" rel="stylesheet">';
                break;
            case 'about':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/about.css" rel="stylesheet">';
                break;
            case 'gallery':
                echo '<link href="'.$path.'resources/views/themes/'.$arrayBase["themes"].'/public/css/gallery.css" rel="stylesheet">';
                break;
            case 'services':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/service.css" rel="stylesheet">';
                break;
            case 'contact':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/contact.css" rel="stylesheet">';
                break;
            case 'login':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/login.css" rel="stylesheet">';
                break;
            case 'register':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/register.css" rel="stylesheet">';
                break;
            case 'blog':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/blog.css" rel="stylesheet">';
                break;
            case 'userinfor':
                echo '<link href="resources/views/themes/'.$arrayBase["themes"].'/public/css/userinfo.css" rel="stylesheet">';
                break;
            case 'gallery/image': 
                echo '<link href="'.$path.'resources/views/themes/'.$arrayBase["themes"].'/public/css/gallery.css" rel="stylesheet">';
                break;
            default:
                echo '<link href="../resources/views/themes/'.$arrayBase["themes"].'/public/css/blog.css" rel="stylesheet">';
                break;
            
        }
    ?>
    
    <script src="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/js/jquery.js')}}"></script>
    <script src="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/js/javascript.js')}}"></script>
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/css/unite-gallery.css')}}"/>
    
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/css/footer.css')}}"/>
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/css/full-slider.css')}}"/>
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/assets/nivoslider/themes/custom/custom.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('resources/views/themes/'.$arrayBase['themes'].'/public/assets/nivoslider/nivo-slider.css')}}" type="text/css" media="screen"/>
	<link href="https://db.onlinewebfonts.com/c/e788d8c87cd314c2f79d9d8db10269b7?family=Six+Caps" rel="stylesheet" type="text/css"/>
	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body onload="randomNumber()">
	<div id="load_screen">
		<div id="loading">
		
		</div>
	</div>
	<div id="page" class="site">
	
        <!-- ------------------------------------Layout header ------------------------- -->
		<header id="masthead" class="site-header centered container" role="banner">
            @include('themes.'.$arrayBase['themes'].'.header')
		</header>
        
        <!-- ------------------------------------End Layout header ------------------------- -->
        
        <!-- ------------------------------------Layout content ------------------------- -->
        
		<div id="content" class="site-content">
            @yield('content')
		</div>
        
         
        <!-- ------------------------------------End Layout content ------------------------- -->
        
        <!-- ------------------------------------Layout footer ------------------------- -->
        <footer id="colophon" class="site-footer default container" role="contentinfo">
            @include('themes.'.$arrayBase['themes'].'.footer')
        </footer>
        <!-- ------------------------------------End layout footer ------------------------- -->
	</div>
		
	
</body>
</html>