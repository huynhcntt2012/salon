<div class="top-panel"> 
				<div class="top-panel__wrap container">
					<div class="top-panel__message">
						<div class="info-block">
							<i class="glyphicon glyphicon-earphone"></i> Call us today: <a href="tel:1-555-644-5566">1-555-644-5566</a>
						</div>
						<div class="info-block">
							<i class="glyphicon glyphicon-map-marker"></i> 7087 Richmond hwy, Alexandria, VA
						</div>
					</div>
					<div class="top-panel__search">
						<form role="search" method="get" class="search-form" action="">
							<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
							</label>
							<button type="submit" class="search-form__submit btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="header-container">
				<div class="header-container_wrap container">
					<div class="header-container__flex">
						<div class="header-container__center">
							<div class="site-branding">
								<div class="site-logo">
									<a class="site-logo__link" href="#" rel="home">Durand</a>
								</div>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<button class="menu-toggle" aria-controls="main-menu" aria-expanded="false">
								<i class="menu-toggle__icon fa fa-bars" aria-hidden="true"></i>
								Menu </button>
								<ul id="main-menu" class="menu">
                                    <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1163"><a href="<?php echo $path ?>home">Home</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-414" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-414">
                                <a href="<?php echo $path ?>about">about us</a>
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a>Gallery</a>
                                    <ul class="sub-menu">
        								<li id="menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="<?php echo $path ?>gallery/image">Image</a></li>
        								<li id="menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="<?php echo $path ?>gallery/video">Video</a></li>
								    </ul>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="<?php echo $path ?>services">Services</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="<?php echo $path ?>blog">blog</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="<?php echo $path ?>contact">Contact</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                   
                                    <?php
                                        if(Session::has('userinfor')){
                                            echo '<a href="'.$path.'userinfor">Profile</a>';
                                        }else{
                                            echo '<a href="'.$path.'login">Login</a>';
                                        }
                                    ?>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
								</ul> 
							</nav>
						</div>
					</div>
				</div>
			</div>