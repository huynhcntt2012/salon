<div id="sidebar-primary" class="col-md-12 col-lg-3 sidebar-primary widget-area" role="complementary">
    <aside id="search-2" class="widget widget_search"><h4 class="widget-title">Search</h4>
        <form role="search" method="get" class="search-form" action="#">
        <label>
            <span class="screen-reader-text">Search for:</span>
            <input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
        </label>
        <button type="submit" class="search-form__submit btn btn-primary">
            <i class="glyphicon glyphicon-search"></i>
        </button>
        </form>
    </aside>
    <aside id="durand_widget_subscribe_follow-3" class="widget widget-subscribe">
        <div class="subscribe-block">
            <h4 class="widget-title">Get Our special offers</h4>
            <h4 class="subscribe-block__message">and the ultimate haircare tips &amp; tricks!</h4>
            <form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="f4e2a07765"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/news/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
                <div class="subscribe-block__success hidden">You successfully subscribed</div>
                <div class="subscribe-block__error hidden"></div>
                </div>
            </form>
        </div>
        <div class="follow-block">
            <h4 class="widget-title">Let's Stay Connected</h4>
            <div class="social-list social-list--widget social-list--icon">
                <ul id="social-list-1" class="social-list__items inline-list">
                    <li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12">
                        <a href="https://www.facebook.com/TemplateMonster/">
                            <span class="screen-reader-text">Facebook</span>
                        </a>
                    </li>
                    <li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13">
                        <a href="https://twitter.com/TemplateMonster">
                            <span class="screen-reader-text">Twitter</span>
                        </a>
                    </li>
                    <li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">
                        <a href="https://plus.google.com/+TemplateMonster">
                            <span class="screen-reader-text">Google plus</span>
                        </a>
                    </li>
                    <li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16">
                        <a href="https://www.linkedin.com/company/templatemonster-com">
                            <span class="screen-reader-text">Linkedin</span>
                        </a>
                    </li>
                    <li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15">
                        <a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <style scoped="">#durand_widget_subscribe_follow-3 .subscribe-block{background-color:#eaeaea}#durand_widget_subscribe_follow-3 .follow-block{background-color:#dedede}</style>
    </aside>
    
    <aside id="durand_widget_about_author-2" class="widget durand widget-about-author"><h4 class="widget-title">About Durand</h4><div class="about-author"><div class="about-author_avatar"><img class="about-author_img" src="public/images/image46-512x442.jpg" width="250" height="250" alt="avatar"></div><div class="about-author_content"><h4 class="about-author_name">admin</h4><div class="about-author_description">Throughout high school and college, she was famous for her talent of cutting hair and creating unique hairstyles. Shane Doe decided to convert her hobby into a profession, she was most excited to make people feel good about themselves.</div><div class="about-author_btn_box"><a href="#" class="about-author_btn btn">Read More</a></div></div>
    </div>
    </aside><aside id="categories-3" class="widget widget_categories"><h4 class="widget-title">Categories</h4><label class="screen-reader-text" for="cat">Categories</label><select name="cat" id="cat" class="postform">
    <option value="-1">Select Category</option>
    <option class="level-0" value="53">Celebrities</option>
    <option class="level-0" value="52">Colors in Trend</option>
    <option class="level-0" value="55">Events</option>
    <option class="level-0" value="51">Haircuts</option>
    <option class="level-0" value="54">Tips</option>
    <option class="level-0" value="1">Uncategorized</option>
    </select>
    
    </aside><aside id="archives-3" class="widget widget_archive"><h4 class="widget-title">Archives</h4> <label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
    <select id="archives-dropdown-3" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
    <option value="">Select Month</option>
    <option value="#"> May 2016 </option>
    <option value="#"> April 2016 </option>
    <option value="#"> March 2016 </option>
    <option value="#"> February 2015 </option>
    </select>
    </aside> <aside id="recent-posts-2" class="widget widget_recent_entries"> <h4 class="widget-title">Recent Posts</h4> <ul>
    <li>
    <a href="#">How to wash and style curly hair</a>
    </li>
    <li>
    <a href="#">Returning to a natural hair texture</a>
    </li>
    <li>
    <a href="#">Winterish version of rainbow hair?</a>
    </li>
    </ul>
    </aside>
    </div>