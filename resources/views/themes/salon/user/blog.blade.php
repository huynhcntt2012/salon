@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

<div id="content" class="site-content">
    <div class="breadcrumbs">
        <div class="container"><div class="breadcrumbs__title"></div>
            <div class="breadcrumbs__items">
                <div class="breadcrumbs__content">
                    <div class="breadcrumbs__wrap"><div class="breadcrumbs__item">
                        <a href="#" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
                        <div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div>
                        <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">News</span></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="site-content_wrap container">
        <div class="row">
            <div id="primary" class="col-md-12 col-lg-9">
                <main id="main" class="site-main" role="main">
                    <header>
                        <h1 class="page-title screen-reader-text">News</h1>
                    </header>
                        <div class="posts-list posts-list--default one-right-sidebar no-sidebars-before">
                            <?php foreach($post as $post){ ?>
                            <article id="post-206" class="posts-list__item card post-thumbnail--fullwidth post-206 post type-post status-publish format-standard has-post-thumbnail hentry category-tips tag-celebrities tag-events has-thumb">
                                <div class="post-list__item-content">
                                    <figure class="post-thumbnail">
                                        <a href="#"><img class="post-thumbnail__img wp-post-image" src="public/images/<?php echo $post->post_image ?>" alt="How to wash and style curly hair" width="1280" height="510"></a>
                                        </figure> 
                                    <header class="entry-header">
                                    <h4 class="entry-title">
                                        <a href="#" rel="bookmark"><?php echo $post->post_title ?></a></h4>
                                    </header> 
                                     
                                    <div class="entry-content">
                                        
                                    </div> 
                                </div> 
                            <footer class="entry-footer">
                            <a href="detail/<?php echo $post->ID ?>" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
                            </article> 
                            <?php } ?>
                        </div> 
                    <nav class="navigation pagination" role="navigation">
                        <h2 class="screen-reader-text">Posts navigation</h2>
                        <div class="nav-links">
                            @if($currentpage > 1)
                                <a class="next page-numbers" href="?page={{ $currentpage-1 }}"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            @endif
                            @for($i = 0; $i < ($totalpage/5); $i++)
                                @if(($i) == $currentpage)
                                    <span class="page-numbers current">{{ $i+1 }}</span>
                                @else
                                    <a class="page-numbers" href="?page={{ $i+1 }}">{{ $i+1 }}</a>
                                @endif
                            @endfor
                            @if($currentpage < (($totalpage/5) - 1))
                               <a class="next page-numbers" href="?page={{ $currentpage + 1 }}"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            @endif
                            </div>
                    </nav>
                </main> 
                
            </div> 
        @include('themes.'.$arrayBase['themes'].'.menuleft')
        </div> 
        
    </div> 
@stop



	