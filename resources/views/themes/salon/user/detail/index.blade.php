@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

<div id="content" class="site-content">
    <div class="breadcrumbs">
        <div class="container"><div class="breadcrumbs__title"></div>
            <div class="breadcrumbs__items">
                <div class="breadcrumbs__content">
                    <div class="breadcrumbs__wrap"><div class="breadcrumbs__item">
                        <a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
                        <div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div>
                        <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">News</span></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="site-content_wrap container">
        <div class="row">
            <div id="primary" class="col-md-12 col-lg-9">
                <main id="main" class="site-main" role="main">
                    <header>
                        <h1 class="page-title screen-reader-text">News</h1>
                    </header>
                        <div class="posts-list posts-list--default one-right-sidebar no-sidebars-before">
                            <?php foreach($data as $key => $value){ ?>
                            <article id="post-206" class="posts-list__item card post-thumbnail--fullwidth post-206 post type-post status-publish format-standard has-post-thumbnail hentry category-tips tag-celebrities tag-events has-thumb">
                                <div class="post-list__item-content">
                                    <figure class="post-thumbnail">
                                        <a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="/{{ $arrayBase['themes'] }}/public/images/<?php echo $value->post_image ?>" alt="How to wash and style curly hair" width="1280" height="510"></a>
                                        <div class="post__cats"></div>
                                    </figure> 
                                    <header class="entry-header">
                                    <h4 class="entry-title">
                                        <a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" rel="bookmark"><p><?php echo $value->post_title ?></p></a></h4> </header> 
                                    <div class="entry-content">
                                        <p><?php echo $value->post_content ?></p>
                                    </div> 
                                </div> 
                            
                             <?php } ?>
                        </div> 
                </main> 
                
            </div> 
        @include('themes.'.$arrayBase['themes'].'.menuleft')
        </div> 
        
    </div> 
	
  
      
        
@stop
