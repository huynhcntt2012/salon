@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')
<div class="panel-heading">
    <div class="panel-options">
        <ul class="nav nav-tabs">
            <?php 
                $i = 0;
                foreach($category as $cate){
                    
                    $i++;
            ?>
                <li class="<?php if($i == 1){echo 'active';} ?>">
                    <a onclick="getPosition(<?php echo $cate->id ?>)" href="#tab-<?php echo $cate->id ?>" class="init" image-position="<?php echo $cate->id ?>" data-toggle="tab" >{{$cate->name}}</a>
                </li>
            <?php
                }
            ?>
            
        </ul>
    </div>
</div>
<div class="panel-body">
        <div class="tab-content">
            <?php $j = 0;
            foreach($category as $cate){
                $j++ ?>
                <div class="tab-pane active" id="tab-<?php echo $cate->id ?>">
    			    <div id="gallery-<?php echo $cate->id ?>" style="display:none;">
                    <?php
                    foreach($data as $image){
                        if($cate->id == $image->id){
                        ?>
                        <img alt="<?php echo $image->name ?>"
    						 src="../public/images/<?php echo $image->link_image ?>"
    						 data-image="../public/images/<?php echo $image->link_image ?>"
    						 data-description="Preview Image 1 Description">
                    <?php
                        }
                    }
                    ?>
    				</div>
                </div>
            <?php }?>
            
            
        </div>
    </div>
                
<script src="{{asset('public/asset/js/unitegallery.js')}}"></script>
<script src="{{asset('public/asset/js/ug-theme-tiles.js')}}"></script>
	<script type="text/javascript">
		var position = <?php echo $category[0]->id;?>;
		$(document).ready(function(){
			$("#gallery-"+position).unitegallery({
				gallery_theme: "tiles",
				tiles_type:"nested",
				tile_enable_border: true,
                tile_border_width: 0,
                tile_border_color: "#ffffff",
                tile_border_radius: 5,
                tiles_space_between_cols: 10,
			});
		});
		function getPosition(is) {
			this.callGallery(is);
			console.log(is);
		}

		function callGallery(is) {
			var isGallery = document.querySelector('#gallery-' + is).classList.contains('ug-gallery-wrapper');
			if (!isGallery) {
				$("#gallery-" + is).unitegallery({
					gallery_theme: "tiles",
					tiles_type:"nested",
					tile_enable_border: true,
	                tile_border_width: 0,
	                tile_border_color: "#ffffff",
	                tile_border_radius: 5,
	                tiles_space_between_cols: 10,
				});
			}
		}
	</script>
      
        
@stop