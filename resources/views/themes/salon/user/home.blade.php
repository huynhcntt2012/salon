@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

<div class="site-content_wrap">
				<div class="row">
				<div id="primary" class="col-xs-12 col-md-12">
					<main id="main" class="site-main" role="main">
						<article id="post-269" class="post-269 page type-page status-publish hentry no-thumb">
						<header class="entry-header">
							<h1 class="entry-title screen-reader-text">Home</h1> 
						</header> 
						<div class="entry-content">
							<div class="tm_builder_outer_content" id="tm_builder_outer_content">
								<div class="tm_builder_inner_content tm_pb_gutters3">
									 	<div class="tm_pb_section  tm_pb_section_4 tm_pb_with_background tm_section_regular">
										<div class=" row tm_pb_row tm_pb_row_6 tm_pb_row_fullwidth tm_pb_col_padding_reset">
											<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_10 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
												<div id="myCarousel" class="carousel slide">
													<!-- Indicators -->

													<!-- Wrapper for Slides -->
													<div class="carousel-inner">
														<div class="item">
															<!-- Set the first background image using inline CSS below. -->
															<div class="fill" style="background-image:url('public/images/banner1.jpg');"></div>
															<div class="carousel-caption">
															
																	<div class="tm_pb_slide_content">
																	<h2 style="text-align: center;"><em>all of our creations are</em> inspired by you as a person</h2>
																	<p style="text-align: center;">Keratin Treatments, Perms/Relaxers, Corrective Color and Extensions Based Upon Consultation</p>
																	<p>&nbsp; </p></div>
																	<a href="" class="tm_pb_more_button tm_btn_1 tm_pb_button">Book an Appointment</a>
															</div>  
														</div>
														<div class="item">
															<!-- Set the second background image using inline CSS below. -->
															<div class="fill" style="background-image:url('public/images/banner2.jpg');"></div>
															<div class="carousel-caption">
																<div class="tm_pb_slide_content">
																	<h2 style="text-align: center;"><em>all of our creations are</em> inspired by you as a person</h2>
																	<p style="text-align: center;">Keratin Treatments, Perms/Relaxers, Corrective Color and Extensions Based Upon Consultation</p>
																	<p>&nbsp; </p></div>
																	<a href="" class="tm_pb_more_button tm_btn_1 tm_pb_button">Book an Appointment</a>
															</div>
														</div>
														<div class="item active">
															<!-- Set the third background image using inline CSS below. -->
															<div class="fill" style="background-image:url('public/images/banner3.jpg');"></div>
															<div class="carousel-caption">
																<div class="tm_pb_slide_content">
																	<h2 style="text-align: center;"><em>all of our creations are</em> inspired by you as a person</h2>
																	<p style="text-align: center;">Keratin Treatments, Perms/Relaxers, Corrective Color and Extensions Based Upon Consultation</p>
																	<p>&nbsp; </p></div>
																	<a href="" class="tm_pb_more_button tm_btn_1 tm_pb_button">Book an Appointment</a>
															</div>
														</div>
													</div>

													<!-- Controls -->
													<a class="left carousel-control custom" href="#myCarousel" data-slide="prev">
														<span class="icon-prev"></span>
													</a>
													<a class="right carousel-control custom" href="#myCarousel" data-slide="next">
														<span class="icon-next"></span>
													</a>

												</div></div>
										</div>  
									</div>
									<div class="tm_pb_section  tm_pb_section_1 tm_section_regular tm_section_transparent">
										<div class="container">
										<div class=" row tm_pb_row tm_pb_row_1">
										<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_1 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
										<hr class="tm_pb_module tm_pb_space tm_pb_divider_3">
										</div>  
										</div>  
										</div><div class="container">
										<div class=" row tm_pb_row tm_pb_row_2">
										<div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_2 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="tm_pb_module tm-waypoint tm_pb_image tm_pb_animation_off accent_big_right tm_pb_image_0 tm_always_center_on_mobile">
										<img src="public/images/home2.jpg" alt="">
										</div>
										</div>  <div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_3 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<hr class="tm_pb_module tm_pb_space extra_devider tm_pb_divider_4"><div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_2">
										<h2>Welcome<br>
										<em> to the best hairdressing salon in New York!</em></h2>
										</div>  <div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_3">
										<p>This salon, founded by three best friends Mary, Brenda and Alberta is now the New York’s women staple for either getting a nice and beautiful hairdo, chit chatting with their favorite stylist or just to feel special again. We’re proud that within just a few years after we opened up our doors, the salon became so popular! In large we’re sure that our incredible, artistic and tasteful stylists and our down-to-earth, non-NYC price range contributed to that fact.</p>
										</div>  <hr class="tm_pb_module tm_pb_space tm_pb_divider_5"><div class="tm_pb_button_module_wrapper tm_pb_module">
										<a class="tm_pb_button tm_pb_custom_button_icon  tm_pb_button_1 tm_pb_module tm_pb_bg_layout_light tm_pb_icon_right" href="#" data-icon="">Read More</a>
										</div>
										</div>  
										</div>  
										</div>
									</div>  
									<div class="tm_pb_section  tm_pb_section_2 tm_section_regular tm_section_transparent">
										<div class="container">
											<div class=" row tm_pb_row tm_pb_row_3">
											<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_4 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_4">
											<h2 style="text-align: center;">Hairdressing <em>Services</em></h2>
											</div>  <hr class="tm_pb_module tm_pb_space tm_pb_divider_6">
											</div>  
										</div>  
										</div>
										<div class="container">
											<div class="row tm_pb_row tm_pb_row_4 tm_pb_col_padding_reset tm_pb_row_4col">
												
                                                <?php foreach($datas as $key => $value){ ?>
                                                <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_5 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
													<div class="tm_pb_link_box tm_pb_link_box_0 tm_pb_module"><div class="tm_pb_link_box_wrap">
													<figure>
                                                        <img src="public/images/<?php echo $value->post_image ?>" alt="">
                                                        <h2>A Movie in the Park:<br />Kung Fu Panda</h2>
                                                    </figure>
													<div class="tm_pb_link_box_content">
                                                            	<h3 class="tm_pb_link_box_title"><a href="#"></a></h3> <div class="tm_pb_blurb_content"> </div>
                                                            	<div class="tm_pb_link_box_button_holder"><a class="tm_pb_button" href="detail/<?php echo $value->ID ?>">Service Details</a></div>
                                                        	</div>
													</div>  
													</div> 
												</div> 
												<?php } ?>
                                                  
											</div>  
										</div>
									</div>  
									  
									
									<div class="tm_pb_section  tm_pb_section_5 tm_pb_with_background tm_section_regular">
    									<div class="container">
        									<div class=" row tm_pb_row tm_pb_row_7">
            									<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_11 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                									<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title-descr tm_pb_text_6">
                    									<h2 style="text-align: center;">our <em>Stylists</em></h2>
                    									<p style="text-align: center;">Our team of professional stylists will never let you choose a bad haircut and will make sure that &nbsp;your appointment will be finished with a smile!</p>
                									</div>
                                                    <hr class="tm_pb_module tm_pb_space tm_pb_divider_8">
            									</div>  
    									</div>  
									</div>
                                    
                                    <div class="container">
									<div class=" row tm_pb_row tm_pb_row_8 tm_pb_col_padding_reset">
									<div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_12 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
									<div class="tm_pb_custom_team_member tm_pb_custom_team_member_0 tm_pb_bg_layout_light alignment_left clearfix tm_pb_module"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/brenda-mayers/" class="tm_pb_team_member_image"><div class="member_image" style="background-image:url(public/images/home11.jpg);"></div></a>
									<div class="tm_pb_team_member_wrap">
									<h3 class="tm_pb_team_member_name"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/brenda-mayers/">Brenda mayers</a></h3>
									<h6 class="tm_pb_member_position">Owner/Head Stylist</h6>
									<div class="tm_pb_team_member_description">
									<p>Brenda is one of the three good friends, who once decided to found &nbsp;the best hairdressing salon in the entire Manhattan!</p>
									</div>  
									<ul class="tm_pb_member_social_links"><li>
									<a href="https://www.facebook.com/TemplateMonster/" class="tm_pb_font_icon tm_pb_facebook_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Facebook</span>
									</a>
									</li><li>
									<a href="https://twitter.com/templatemonster" class="tm_pb_font_icon tm_pb_twitter_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Twitter</span>
									</a>
									</li><li>
									<a href="https://plus.google.com/+TemplateMonster" class="tm_pb_font_icon tm_pb_google_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Google+</span>
									</a>
									</li></ul></div>  
									</div> 
									</div>  <div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_13 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
									<div class="tm_pb_custom_team_member tm_pb_custom_team_member_1 tm_pb_bg_layout_light alignment_left clearfix tm_pb_module"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/stacy-waters/" class="tm_pb_team_member_image"><div class="member_image" style="background-image:url(public/images/home12.jpg);"></div></a>
									<div class="tm_pb_team_member_wrap">
									<h3 class="tm_pb_team_member_name"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/stacy-waters/">Stacy waters</a></h3>
									<h6 class="tm_pb_member_position">Stylist</h6>
									<div class="tm_pb_team_member_description">
									<p>Stacy is a vital part of our team, as she’s the youngest and most up-to-date, vibrant hair stylist among us. She’s the heart and soul of our salon.</p>
									</div>  
									<ul class="tm_pb_member_social_links"><li>
									<a href="https://www.facebook.com/TemplateMonster/" class="tm_pb_font_icon tm_pb_facebook_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Facebook</span>
									</a>
									</li><li>
									<a href="https://twitter.com/templatemonster" class="tm_pb_font_icon tm_pb_twitter_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Twitter</span>
									</a>
									</li><li>
									<a href="https://plus.google.com/+TemplateMonster" class="tm_pb_font_icon tm_pb_google_icon">
									<span class="tm-pb-icon" data-icon=""></span>
									<span class="tm-pb-tooltip">Google+</span>
									</a>
									</li></ul></div>  
									</div> 
									</div>  
									</div>  
									</div>
									<div class="container">
										<div class=" row tm_pb_row tm_pb_row_9 tm_pb_col_padding_reset">
										<div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_14 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="tm_pb_custom_team_member tm_pb_custom_team_member_2 tm_pb_bg_layout_light alignment_right clearfix tm_pb_module"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/alicia-peace/" class="tm_pb_team_member_image"><div class="member_image" style="background-image:url(public/images/home13.jpg);"></div></a>
										<div class="tm_pb_team_member_wrap">
										<h3 class="tm_pb_team_member_name"><a href="">Alicia Peace</a></h3>
										<h6 class="tm_pb_member_position">Stylist</h6>
										<div class="tm_pb_team_member_description">
										<p>While her main work focus is&nbsp;children’s haircuts, Alicia is good basically at anything she does. Just ask her returning clients!</p>
										</div>  
										<ul class="tm_pb_member_social_links"><li>
										<a href="https://www.facebook.com/TemplateMonster/" class="tm_pb_font_icon tm_pb_facebook_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Facebook</span>
										</a>
										</li><li>
										<a href="https://twitter.com/templatemonster" class="tm_pb_font_icon tm_pb_twitter_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Twitter</span>
										</a>
										</li><li>
										<a href="https://plus.google.com/+TemplateMonster" class="tm_pb_font_icon tm_pb_google_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Google+</span>
										</a>
										</li></ul></div>  
										</div> 
										</div>  <div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_15 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
										<div class="tm_pb_custom_team_member tm_pb_custom_team_member_3 tm_pb_bg_layout_light alignment_right clearfix tm_pb_module"><a href="" class="tm_pb_team_member_image"><div class="member_image" style="background-image:url(public/images/home14.jpg);"></div></a>
										<div class="tm_pb_team_member_wrap">
										<h3 class="tm_pb_team_member_name"><a href="">Mary Pipestone</a></h3>
										<h6 class="tm_pb_member_position">Stylist</h6>
										<div class="tm_pb_team_member_description">
										<p>One of our co-founders, Mary is the most creative of our stylists’ team. Besides hair styling and coloring, she also makes hair extensions.</p>
										</div>  
										<ul class="tm_pb_member_social_links"><li>
										<a href="https://www.facebook.com/TemplateMonster/" class="tm_pb_font_icon tm_pb_facebook_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Facebook</span>
										</a>
										</li><li>
										<a href="https://twitter.com/templatemonster" class="tm_pb_font_icon tm_pb_twitter_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Twitter</span>
										</a>
										</li><li>
										<a href="https://plus.google.com/+TemplateMonster" class="tm_pb_font_icon tm_pb_google_icon">
										<span class="tm-pb-icon" data-icon=""></span>
										<span class="tm-pb-tooltip">Google+</span>
										</a>
										</li></ul></div>  
										</div> 
										</div>  
										</div>  
									</div>
									</div>  
                                    
                                    
                                    <div class="tm_pb_section  tm_pb_section_6 tm_section_regular tm_section_transparent">
									<div class="container">
										<div class=" row tm_pb_row tm_pb_row_10">
    										<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_16 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        										<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_7">
        										  <h2 style="text-align: center;">See what’s new o<em>n our Blog</em></h2>
        										</div>
                                                <div class="tm_pb_blog tm_pb_blog_0 tm_pb_module tm_pb_bg_layout_light tm_pb_posts clearfix layout-grid tm_pb_module">
                                                <div class="tm_pb_blog_grid_wrapper row" data-columns="">
                                                   
                                                    
                                                    
                                                    <?php foreach($datab as $kay => $value){ ?>
                                                    
                                                    <div class="col-xl-4 col-lg-3 col-md-6 col-sm-12">
                                                        <article id="post-202" class="tm_pb_post post-202 post type-post status-publish format-standard has-post-thumbnail hentry category-events tag-events tag-haircuts has-thumb">
                    										<div class="tm_pb_post_meta">
                                                                <strong class="published">April 21</strong><span>Posted by <span class="author vcard"><a title="Posts by admin">admin</a></span></span><span class="comments"></span>
                                                            </div>
                                                            <div class="tm_pb_image_container"> <a href="#" class="entry-featured-image-url">
              										            <img width="536" height="272" src="public/images/<?php echo $value->post_image ?>" class="attachment-durand-thumb-536-272 size-durand-thumb-536-272 wp-post-image" alt="Winterish version of rainbow hair?"> </a>
                    										</div>
                                                            <h3 class="entry-title">
                                                                <a href="#"><?php echo $value->post_title ?></a>
                                                            </h3>
                                                            <a href="detail/<?php echo $value->ID ?>" class="more-link">Read more</a>
                                                        </article>
                                                    </div>
                                                    <?php } ?>
                                                    
                                                    </div>
                                                </div> 
    										</div>  
										</div>  
									</div>
									</div> 
									<div class="tm_pb_section  tm_pb_section_7 tm_pb_with_background tm_section_regular">
									<div class="container">
										<div class=" row tm_pb_row tm_pb_row_11">
    										<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_17 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        										<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_8">
        				                            <h2 style="text-align: center;">follow us&nbsp;<em>on instargam #durand</em></h2>
        										</div>
                                                    <hr class="tm_pb_module tm_pb_space tm_pb_divider_9">
                                                    <div class="tm_pb_widget_area custom-area tm_pb_widget_area_left clearfix tm_pb_module tm_pb_bg_layout_light main-title tm_pb_sidebar_0">
                										<aside id="durand_widget_instagram-2" class="widget widget-instagram">
                                                            <div class="instagram__items">
                        										<div class="instagram__item">
                                                                    <a class="instagram__link" href="#" target="_blank" rel="nofollow"><img class="instagram__img" src="public/images/footer1.jpg" alt="" width="350" height="350"><span class="instagram__cover"></span></a> <div class="instagram__caption"><h6></h6></div>
                        										</div>
                        										<div class="instagram__item">
                                                                    <a class="instagram__link" href="#" target="_blank" rel="nofollow"><img class="instagram__img" src="public/images/footer2.jpg" alt="" width="350" height="350"><span class="instagram__cover"></span></a> <div class="instagram__caption"><h6></h6></div>
                        										</div>
                        										<div class="instagram__item">
                                                                    <a class="instagram__link" href="#" target="_blank" rel="nofollow"><img class="instagram__img" src="public/images/footer3.jpg" alt="" width="350" height="350"><span class="instagram__cover"></span></a> <div class="instagram__caption"><h6></h6></div>
                        										</div>
                        										<div class="instagram__item">
                                                                    <a class="instagram__link" href="#" target="_blank" rel="nofollow"><img class="instagram__img" src="public/images/footer4.jpg" alt="" width="350" height="350"><span class="instagram__cover"></span></a> <div class="instagram__caption"><h6></h6></div>
                        										</div>
                        										<div class="instagram__item">
                                                                    <a class="instagram__link" href="#" target="_blank" rel="nofollow"><img class="instagram__img" src="public/images/footer5.jpg" alt="" width="350" height="350"><span class="instagram__cover"></span></a> <div class="instagram__caption"><h6></h6></div>
                        										</div>
                                                            </div>
                                                        </aside>
        										  </div>  
    										</div>  
										</div>  
									</div>
									</div>  
								</div>
							</div> 
						</div> 
						<footer class="entry-footer">
						</footer> 
						</article> 
					</main> 
				</div> 
				</div> 
			</div>
      
        
@stop



