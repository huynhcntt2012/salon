@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs__title"></div>
        <div class="breadcrumbs__items">
            <div class="breadcrumbs__content">
                <div class="breadcrumbs__wrap">
                    <div class="breadcrumbs__item">
                        <a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Trang Chủ</a>
                    </div>
                    <div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div>
                    <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Đăng Ký</span></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="site-content_wrap container">
    <div class="row">
        <div id="primary" class="col-md-12 col-lg-9">
            <main id="main" class="site-main" role="main">
                <header>
                    <h1 class="page-title screen-reader-text">Register</h1>
                </header>
                <article id="post-77" class="entry author- post-77 page type-page status-publish" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
                    <header class="entry-header">
                        <h1 class="entry-title font-headlines" itemprop="headline">Đăng Ký Thành Viên</h1>
                    </header>
                    <div class="entry-content" itemprop="text">
                        <div class="login mu_register" id="theme-my-login0">
                            <h2 class="font-headlines">Thông Tin Khách Hàng</h2> 
                            <form id="setupform" method="post" action="register" onsubmit="return checkRegister()"> 
                                <input type="hidden" name="action" value="register" class="font-primary"> 
                                <input type="hidden" name="stage" value="validate-user-signup" class="font-primary"> 
                                <input type="hidden" name="signup_form_id" value="1273454977" class="font-primary">
                                <input type="hidden" id="_signup_form" name="_signup_form" value="755cc3d799" class="font-primary"> 
                                <label for="user_name" class="font-secondary label-text">Tên Khách Hàng:</label> 
                                <input name="user_name" type="text" id="user_name" value="" maxlength="60" class="font-primary">
                                <br> 
                                <span id="inputuser" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                <label for="phone" class="font-secondary label-text">Số&nbsp;Điện&nbsp;Thoại:</label> 
                                <input name="phone" type="text" id="phone" value="" maxlength="200" class="font-primary">
                                <br>
                                <span id="inputpassconform" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                <label for="birthday" class="font-secondary label-password">Ngày Sinh:</label> 
                                <input autocomplete="on" name="birthday" id="birthday" class="form-control" size="20" value="" type="date">
                                <br>
                                <span id="inputphone" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                <label for="pass1" class="font-secondary label-password">Mật Khẩu:</label> 
                                <input autocomplete="off" name="pass1" id="pass1" class="input font-primary" size="20" value="" type="password">
                                <br> 
                                <span id="inputpass" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                <label for="pass2" class="font-secondary label-password">Xác Nhận Mật Khẩu:</label> 
                                <input autocomplete="off" name="pass2" id="pass2" class="input font-primary" size="20" value="" type="password">
                                <br> 
                                <span id="inputpassconform" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                <label for="address" class="font-secondary label-password">Địa Chỉ:</label> 
                                <input autocomplete="off" name="address" id="address" class="input font-primary" size="20" value="" type="text">
                                <br>
                                <span class="hint"></span>
                                <div class="cptch_block"> <span id="cptch_time_limit_notice_61" class="cptch_time_limit_notice cptch_to_remove" style="display: block;"></span>
                                    <span class="cptch_wrap">
                                        <label  class="cptch_label font-secondary label-text label-hidden" for="cptch_input_61"> 
                                            <span id="paraa" name="paraa" class="cptch_span">10</span> 
                                            <span class="cptch_span">&nbsp;+&nbsp;</span> <span class="cptch_span">
                                            <input  id="cptch_input_61" class="cptch_input  font-primary" type="text" autocomplete="off" name="cptch_number" value="" maxlength="1" size="1" aria-required="true" required="required" style="margin-bottom:0;display:inline;font-size: 12px;width: 40px;"></span> 
                                            <span class="cptch_span">&nbsp;=&nbsp;</span> 
                                            <span id="result" name="result" class="cptch_span">10</span> 
                                            <input type="hidden" name="cptch_result" value="RBk=" class="font-primary"> 
                                            <input type="hidden" name="cptch_time" value="1486923519" class="font-primary"> 
                                            <input type="hidden" name="cptch_form" value="wp_register" class="font-primary"> 
                                        </label>
                                        <span class="cptch_reload_button_wrap hide-if-no-js">
                                            <noscript>&lt;style type="text/css"&gt;.hide-if-no-js {
                                            					display: none !important;
                                            				}&lt;/style&gt;
                                            </noscript> 
                                            <span class="btnrefresh glyphicon glyphicon-refresh"></span> 
                                        </span>
                                    </span>
                                </div>
                                <br>
                                <p>
                                    <input id="signupblog" type="hidden" name="signup_for" value="user" class="font-primary">
                                </p>
                                <p class="submit">
                                    <input type="submit" name="submit" class="submit font-primary font-secondary" value="Next">
                                </p>
                            </form>
                        <ul class="tml-action-links">
                            <li>
                                <a href="login" rel="nofollow">Log In</a>
                            </li>
                            <li>
                                <a href="lostpassword" rel="nofollow">Lost Password</a>
                            </li>
                        </ul>
                        </div> 
                    </div>
                    <footer class="entry-footer font-secondary"></footer>   
                </article>
                </main> 
        </div> 
    @include('themes.'.$arrayBase['themes'].'.menuleft')
    </div> 
</div> 
  
      
        
@stop





		